# Copyright 2012,2014 Bernd Steinhauser <berniyh@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=CDrummond release=v${PV} suffix=tar.bz2 ] cmake
require gtk-icon-cache
require ffmpeg [ with_opt=true option_name=replaygain ]

SUMMARY="Qt client for the music player daemon (mpd)"
DESCRIPTION="
Cantata is a Qt5 client for the music player daemon (mpd). It supports:
* Configurable interface
* Library
* Dynamic playlists
* Saving internet radio streams
* Lyrics
* Info about titles can be fetched from wikipedia using webkit
* mpd statistics
* Handling of usb and mtp devices to copy from or onto external devices
"

LICENCES="GPL-3"
SLOT="0"
MYOPTIONS="
    avahi [[ description = [ Enable automatic mpd server discovery ] ]]
    http  [[ description = [ Enable playback of MPD HTTP streams ] ]]
    mtp   [[ description = [ Support handling of external MTP devices (i.e. portable media players) ] ]]
    replaygain [[ description = [ Support replaygain via mpg123 and ffmpeg ] ]]

    ( ( providers: eudev systemd ) [[ number-selected = exactly-one ]] )
"

QT_MIN_VER="5.11"

DEPENDENCIES="
    build:
        sys-devel/gettext
        virtual/pkg-config
        x11-libs/qttools:5 [[ note = [ lrelease for translations ] ]]
    build+run:
        media-libs/taglib:=[>=1.6]
        sys-libs/zlib
        x11-libs/qtbase:5[>=${QT_MIN_VER}][gui][sql]
        x11-libs/qtsvg:5[>=${QT_MIN_VER}]
        avahi? ( net-dns/avahi )
        http? ( x11-libs/qtmultimedia:5[>=${QT_MIN_VER}] )
        mtp? ( media-libs/libmtp )
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
        replaygain? (
            media-libs/libebur128
            media-sound/mpg123
        )
    run:
        dev-lang/perl:*[ithreads(+)] [[ note = [ Needed for dynamic playlist generator ] ]]
"

CMAKE_SRC_CONFIGURE_OPTION_ENABLES+=(
    AVAHI
    'http HTTP_STREAM_PLAYBACK'
    'replaygain FFMPEG'
    'replaygain MPG123'
    MTP
)

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DCMAKE_FIND_ROOT_PATH:PATH="$(ffmpeg_alternatives_prefix);/usr/$(exhost --target)"
    -DLRELEASE_EXECUTABLE:STRING=lrelease-qt5
    -DSHARE_INSTALL_PREFIX:PATH="/usr/share"
    -DENABLE_CDIOPARANOIA=Off
    -DENABLE_CDPARANOIA=Off
    # Can be used as an alternative for http stream playback
    -DENABLE_LIBVLC=Off
    -DENABLE_KWALLET=Off
)

