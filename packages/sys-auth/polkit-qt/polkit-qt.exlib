# Copyright 2009, 2010 Ingmar Vanhassel
# Copyright 2014 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PN="${PN}-1"
MY_PNV="${MY_PN}-${PV}"

require kde.org [ subdir=${MY_PN} ] cmake

export_exlib_phases src_prepare src_configure src_compile src_install

SUMMARY="A library that allows developer to access PolicyKit-1 API with a nice Qt-style API"
HOMEPAGE="https://commits.kde.org/${MY_PN}"

LICENCES="LGPL-2.1"
SLOT="1"

MYOPTIONS="
    examples
"

if ever at_least 0.115.0 ; then
    MYOPTIONS+="
        ( providers: qt5 qt6 ) [[ number-selected = at-least-one ]]
    "
fi

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        sys-auth/polkit:1[>=0.99]
    suggestion:
        sys-auth/polkit-kde-agent[>=0.99.0]     [[ description = [ KDE authentication GUI for PolicyKit ] ]]
"

if ever at_least 0.115.0 ; then
    DEPENDENCIES+="
        build+run:
            dev-libs/glib:2[>=2.36]
            providers:qt5? ( x11-libs/qtbase:5[>=5.5.0] )
            providers:qt6? ( x11-libs/qtbase:6 )
    "
else
    DEPENDENCIES+="
        build+run:
            dev-libs/glib:2
            x11-libs/qtbase:5[>=5.5.0]
    "
fi

_polkit-qt_conf() {
    local cmake_params=(
        -DSYSCONF_INSTALL_DIR:PATH=/etc/
        -DBUILD_TEST:BOOL=TRUE
        $(cmake_build EXAMPLES)
        "$@"
    )

    ecmake "${cmake_params[@]}"
}


polkit-qt_src_prepare() {
    cmake_src_prepare

    # TODO: Turn this into a patch sent upstream (heirecka)
    edo sed -e "/POLKITQT-1_POLICY_FILES_INSTALL_DIR/s:\"\${POLKITQT-1_INSTALL_DIR}:\"/usr:" \
            -i PolkitQt-1Config.cmake.in
}

polkit-qt_src_configure() {
    if ever at_least 0.115.0 ; then
        if option providers:qt5 ; then
            edo mkdir "${WORKBASE}"/qt5-build
            edo pushd "${WORKBASE}"/qt5-build
            _polkit-qt_conf -DQT_MAJOR_VERSION=5
            edo popd
        fi

        if option providers:qt6 ; then
            edo mkdir "${WORKBASE}"/qt6-build
            edo pushd "${WORKBASE}"/qt6-build
            _polkit-qt_conf -DQT_MAJOR_VERSION=6
            edo popd
        fi
    else
        cmake_src_configure
    fi
}

polkit-qt_src_compile() {
    if ever at_least 0.115.0 ; then
        if option providers:qt5 ; then
            edo pushd "${WORKBASE}"/qt5-build
            ecmake_build
            edo popd
        fi

        if option providers:qt6 ; then
            edo pushd "${WORKBASE}"/qt6-build
            ecmake_build
            edo popd
        fi
    else
        cmake_src_compile
    fi
}

polkit-qt_src_install() {
    if ever at_least 0.115.0 ; then
        if option providers:qt5 ; then
            edo pushd "${WORKBASE}"/qt5-build
            ecmake_install
            edo popd
        fi

        if option providers:qt6 ; then
            edo pushd "${WORKBASE}"/qt6-build
            ecmake_install
            edo popd
        fi

        edo pushd "${CMAKE_SOURCE}"
        emagicdocs
        edo popd
    else
        cmake_src_install
    fi
}

