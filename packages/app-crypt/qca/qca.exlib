# Copyright 2008 Thomas Anderson
# Copyright 2014-2015, 2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'qca-2.0.0.ebuild', which is:
#   Copyright 1999-2008 Gentoo Foundation

require kde.org cmake

export_exlib_phases src_prepare src_configure src_compile src_test src_install

SUMMARY="Qt Cryptographic Architecture (QCA)"
HOMEPAGE+=" https://userbase.kde.org/QCA"

LICENCES="BSD-3 [[ note = [ cmake scripts ] ]] LGPL-2"
SLOT="2"
MYOPTIONS="
    botan  [[ description = [ Build crypto algorithms plugin based on botan ] ]]
    doc
    examples
    gcrypt [[ description = [ Build crypto algorithms plugin based on libgcrypt ] ]]
    gnupg  [[ description = [ Build crypto algorithms plugin based on gnupg ] ]]
    nss    [[ description = [ Build crypto algorithms plugin based on nss ] ]]
    pkcs11 [[ description = [ Build crypto algorithms plugin based on pkcs11-helper ] ]]
    sasl   [[ description = [ Build crypto algorithms plugin based on cyrus-sasl ] ]]

    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
    ( providers: qt5 qt6 ) [[ number-selected = at-least-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
        doc? ( app-doc/doxygen )
    build+run:
        botan? ( dev-libs/botan:=[>=2] )
        gcrypt? ( dev-libs/libgcrypt )
        gnupg? ( app-crypt/gnupg )
        nss? ( dev-libs/nss )
        pkcs11? ( dev-libs/pkcs11-helper )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:=[>=1.1.1] )
        providers:qt5? ( x11-libs/qtbase:5[>=5.14][?gui(+)] )
        providers:qt6? (
            x11-libs/qtbase:6
            x11-libs/qt5compat:6
        )
        sasl? ( net-libs/cyrus-sasl[>=2] )
        (
            !app-crypt/qca-gnupg:2
            !app-crypt/qca-ossl:2
        ) [[
            *description = [ qca-gnupg and qca-ossl are included in qca>=2.1.0 ]
            *resolution = uninstall-blocked-after
        ]]
"

with_plugin() {
    local opt="${1}" cmake_flag="${2:-${1}}"
    echo "-DWITH_${cmake_flag}_PLUGIN=$(option ${opt} 'yes' 'no')"
}

qca_conf() {
    local cmake_params=(
        -DBUILD_SHARED_LIBS:BOOL=TRUE
        -DQCA_DOC_INSTALL_DIR=/usr/share/doc/${PNV}
        -DQCA_MAN_INSTALL_DIR=/usr/share/man
        -DWITH_ossl_PLUGIN=TRUE
        $(with_plugin botan)
        $(with_plugin gcrypt)
        $(with_plugin gnupg)
        $(with_plugin nss)
        $(with_plugin pkcs11)
        $(with_plugin sasl cyrus-sasl)
        $(expecting_tests -DBUILD_TESTS:BOOL=TRUE -DBUILD_TESTS:BOOL=FALSE )
        "$@"
    )

    ecmake "${cmake_params[@]}"
}

qca_src_prepare() {
    cmake_src_prepare

    if option providers:libressl ; then
        # Disable failing test with LibreSSL for now,
        # https://phabricator.kde.org/D20259 might address this
        edo sed -e '/^add_qca_test(pkits "PublicKeyInfrastructure")/d' \
                -i unittest/pkits/CMakeLists.txt
    fi
}

qca_src_configure() {
    if option providers:qt5 ; then
        edo mkdir "${WORKBASE}"/qt5-build
        edo pushd "${WORKBASE}"/qt5-build
        qca_conf \
            -DQCA_FEATURE_INSTALL_DIR=/usr/$(exhost --target)/lib/qt5/mkspecs/features \
            -DQCA_INCLUDE_INSTALL_DIR=/usr/$(exhost --target)/include/Qca-qt5 \
            -DQCA_PLUGINS_INSTALL_DIR=/usr/$(exhost --target)/lib/qt5/plugins \
            -DQCA_SUFFIX=qt5 \
            -DBUILD_WITH_QT6:BOOL=FALSE
        edo popd
    fi

    if option providers:qt6 ; then
        edo mkdir "${WORKBASE}"/qt6-build
        edo pushd "${WORKBASE}"/qt6-build
        qca_conf \
            -DQCA_FEATURE_INSTALL_DIR=/usr/$(exhost --target)/lib/qt6/mkspecs/features \
            -DQCA_INCLUDE_INSTALL_DIR=/usr/$(exhost --target)/include/Qca-qt6 \
            -DQCA_PLUGINS_INSTALL_DIR=/usr/$(exhost --target)/lib/qt6/plugins \
            -DQCA_SUFFIX=qt6 \
            -DBUILD_WITH_QT6:BOOL=TRUE
        edo popd
    fi
}

qca_src_compile() {
    if option providers:qt5 ; then
        edo pushd "${WORKBASE}"/qt5-build
        ecmake_build
        edo popd
    fi

    if option providers:qt6 ; then
        edo pushd "${WORKBASE}"/qt6-build
        ecmake_build
        edo popd
    fi
}

qca_src_test() {
    if option providers:qt5 ; then
        edo pushd "${WORKBASE}"/qt5-build
        # Test passes, but the build hangs after src_test, I guess gpg-agent
        # stays around
        ectest -E PGP
        edo popd
    fi

    if option providers:qt6 ; then
        edo pushd "${WORKBASE}"/qt6-build
        # FileWatch only fails with Qt6
        ectest -E '(FileWatch|PGP)'
        edo popd
    fi
}

qca_src_install() {
    if option providers:qt5 ; then
        edo pushd "${WORKBASE}"/qt5-build
        ecmake_install
        edo popd
    fi

    if option providers:qt6 ; then
        edo pushd "${WORKBASE}"/qt6-build
        ecmake_install
        edo popd
    fi

    if option doc ; then
        edo doxygen "${CMAKE_SOURCE}"/Doxyfile.in
        dodoc apidocs/html/*
    fi

    if option examples ; then
        pushd ${CMAKE_SOURCE}/examples
        docinto examples
        dodoc -r *
    fi

    edo pushd "${CMAKE_SOURCE}"
    emagicdocs
    edo popd
}

