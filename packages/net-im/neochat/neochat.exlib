# Copyright 2020-2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
myexparam kf5_min_ver
myexparam qt_min_ver

exparam -v major_version kf_major_version

require kde-apps [ group_or_user=network ]
require kde [ kf_major_version=${major_version} translations=ki18n ]
require freedesktop-desktop gtk-icon-cache
require xdummy [ phase=test ]

export_exlib_phases src_test pkg_postinst pkg_postrm

SUMMARY="A client for Matrix, the decentralized communication protocol for IM"
DESCRIPTION="
It is a fork of Spectral, using KDE frameworks, most notably Kirigami, KConfig
and KI18n."

LICENCES="BSD-2 CC0 GPL-2 GPL-3 LGPL-2.1 LGPL-3"
SLOT="0"
MYOPTIONS=""

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:${major_version}[>=${KF5_MIN_VER}]
        virtual/pkg-config
    build+run:
        app-text/cmark:=
        kde-frameworks/kconfig:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kio:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kitemmodels:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/sonnet:${major_version}[>=${KF5_MIN_VER}]
        x11-libs/qtbase:${major_version}[>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:${major_version}[>=${QT_MIN_VER}]
        x11-libs/qtmultimedia:${major_version}[>=${QT_MIN_VER}]
        x11-libs/qtsvg:${major_version}[>=${QT_MIN_VER}]
    run:
        kde-frameworks/kitemmodels:${major_version}[>=${KF5_MIN_VER}] [[ note = [ org.kde.kitemmodels ] ]]
        kde-frameworks/kquickcharts:${major_version}[>=${KF5_MIN_VER}] [[ note = [ org.kde.quickcharts ] ]]
        x11-libs/qtlocation:${major_version}[>=${QT_MIN_VER}] [[ note = [ QtLocation and QtPositioning ] ]]
"

if ever at_least 24.01.75 ; then
    DEPENDENCIES+="
        build+run:
            kde-frameworks/kcrash:${major_version}[>=${KF5_MIN_VER}]
            kde-frameworks/purpose:${major_version}[>=${KF5_MIN_VER}]
            kde-frameworks/syntax-highlighting:${major_version}[>=${KF5_MIN_VER}]
    "
    DEPENDENCIES+="
        run:
            kde-frameworks/syntax-highlighting:${major_version}[>=${KF5_MIN_VER}] [[ note = [ org.kde.kitemmodels ] ]]
    "
fi

if ever at_least 24.01.75 ; then
    DEPENDENCIES+="
        build+run:
            dev-libs/icu:=[>=61.0]
            dev-libs/qcoro[>=0.4][providers:qt6]
            kde-frameworks/kcolorscheme:${major_version}[>=${KF5_MIN_VER}]
            kde-frameworks/kirigami:${major_version}[>=${KF5_MIN_VER}]
            kde-frameworks/kstatusnotifieritem:${major_version}[>=${KF5_MIN_VER}]
            net-im/libquotient:=[>=0.7][e2ee][providers:qt6]
            sys-auth/qtkeychain[providers:qt6]
            x11-libs/qtwebview:${major_version}[>=${QT_MIN_VER}]
        run:
            kde/kirigami-addons:6[>=0.7.2]
            kde/kquickimageeditor[>=0.3]
            kde-frameworks/qqc2-desktop-style:${major_version}[>=${KF5_MIN_VER}]
            kde-frameworks/prison:${major_version}[>=${KF5_MIN_VER}]   [[ note = [ org.kde.prison ] ]]
    "

    # TODO: 7 of 16 tests fail, but not on upstream CI
    RESTRICT="test"

    CMAKE_SRC_CONFIGURE_PARAMS+=(
        # Unwritten, depends on infra only intended for development purposes
        -DCMAKE_DISABLE_FIND_PACKAGE_KUnifiedPush:BOOL=TRUE
        -DCMAKE_DISABLE_FIND_PACKAGE_SeleniumWebDriverATSPI:BOOL=TRUE
    )
else
    DEPENDENCIES+="
        build+run:
            dev-libs/qcoro[>=0.4][providers:qt5]
            kde-frameworks/kirigami:2[>=${KF5_MIN_VER}]
            net-im/libquotient:=[>=0.7][providers:qt5]
            sys-auth/qtkeychain[providers:qt5]
            x11-libs/qtquickcontrols2:5[>=${QT_MIN_VER}]
        run:
            kde/kirigami-addons:0[>=0.7.2]
            kde/kquickimageeditor[<0.3]
            kde-frameworks/qqc2-desktop-style:0[>=${KF5_MIN_VER}]
    "
fi

neochat_src_test() {
    xdummy_start
    cmake_src_test
    xdummy_stop
}

neochat_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

neochat_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

