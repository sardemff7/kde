# Copyright 2020-2022 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
myexparam kf5_min_ver
myexparam qt_min_ver

exparam -v major_version kf_major_version

require plasma kde [ kf_major_version=${major_version} translations='ki18n' ]

SUMMARY="An application for monitoring system resources"
DESCRIPTION="
plasma-systemmonitor provides an interface for monitoring system sensors,
process information and other system resources. It is built on top of the faces
system also used to provide widgets for plasma-dekstop and makes use of the
ksystemstats daemon to provide sensor information. It allows extensive
customisation of pages, so it can be made to show exactly which data people
want to see."

LICENCES="
    BSD-3
    CC0 [[ note = [ appstream metadata ] ]]
    || ( LGPL-2.1 LGP-3 )
    || ( GPL-2 GPL-3 )
"
SLOT="0"
MYOPTIONS=""

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build+run:
        kde/libksysguard:4[>=$(ever range -3)]
        kde-frameworks/kconfig:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kglobalaccel:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kio:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kitemmodels:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/knewstuff:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kpackage:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kservice:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:${major_version}[>=${KF5_MIN_VER}]
        x11-libs/qtbase:${major_version}[>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:${major_version}[>=${QT_MIN_VER}]
    run:
        kde/ksystemstats[>=5.20]
"

if ever at_least 6.0.90 ; then
    DEPENDENCIES+="
        build+run:
            kde/kirigami-addons:6[>=1.1.0]
    "
fi

if ever at_least 5.91.0 ; then
    DEPENDENCIES+="
        build+run:
            kde/kirigami-addons:6
            kde-frameworks/kirigami:6[>=${KF5_MIN_VER}]
        run:
            kde-frameworks/kiconthemes:6[>=${KF5_MIN_VER}] [[ note = [ org.kde.iconthemes ] ]]
    "
else
    DEPENDENCIES+="
        build+run:
            kde-frameworks/kdeclarative:${major_version}[>=${KF5_MIN_VER}]
            kde-frameworks/kirigami:2[>=${KF5_MIN_VER}]
            x11-libs/qtquickcontrols2:5[>=${QT_MIN_VER}]
    "
fi

