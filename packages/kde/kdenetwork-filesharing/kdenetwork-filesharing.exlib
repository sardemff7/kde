# Copyright 2013, 2015 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'kdenetwork.exlib', which is:
#     Copyright 2008-2009, 2010, 2011, 2012 Ingmar Vanhassel
#     Copyright 2008-2011 Bo Ørsted Andresen

myexparam kf_major_version=5
myexparam kf5_min_ver
myexparam qt_min_ver

exparam -v major_version kf_major_version

require kde-apps kde [ kf_major_version=${major_version} translations='ki18n' ]

SUMMARY="KControl filesharing module for samba"

LICENCES="GPL-2 LGPL-2.1 FDL-1.2"
MYOPTIONS=""

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build+run:
        kde-frameworks/kauth:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcompletion:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kio:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:${major_version}[>=${KF5_MIN_VER}]
        x11-libs/qtbase:${major_version}[>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:${major_version}[>=${QT_MIN_VER}]
    suggestion:
        net-fs/samba [[ description = [ Network file sharing ] ]]
"

if ever at_least 24.01.85 ; then
    DEPENDENCIES+="
        build+run:
            dev-libs/qcoro[providers:qt6]
    "

    # avoid installing samba with PackageKit
    CMAKE_SRC_CONFIGURE_PARAMS+=(
        -DCMAKE_DISABLE_FIND_PACKAGE_PackageKitQt6:BOOL=TRUE
        -DSAMBA_INSTALL=OFF
    )
else
    DEPENDENCIES+="
        build+run:
            dev-libs/qcoro[providers:qt5(+)]
    "

    CMAKE_SRC_CONFIGURE_PARAMS+=(
        -DCMAKE_DISABLE_FIND_PACKAGE_PackageKitQt5:BOOL=TRUE
        -DSAMBA_INSTALL=OFF
    )
fi

