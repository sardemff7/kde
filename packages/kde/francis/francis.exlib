# Copyright 2024 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf6_min_ver
myexparam qt_min_ver

require kde-apps
require kde [ kf_major_version=6 translations=ki18n ]
require gtk-icon-cache

export_exlib_phases src_test

SUMMARY="Track your time with the well-known pomodoro technique"

LICENCES="
    BSD-2 [[ note = [ cmake ] ]]
    BSD-3 [[ note = [ gradle ] ]]
    CC0   [[ note = [ dotfiles, scripts, README, etc ] ]]
    GPL-3
    || ( LGPL-2.0 LGPL-2.1 LGPL-3 )
"
SLOT="0"
MYOPTIONS=""

exparam -v KF6_MIN_VER kf6_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build+run:
        kde-frameworks/kconfig:6[>=${KF6_MIN_VER}]
        kde-frameworks/kcoreaddons:6[>=${KF6_MIN_VER}]
        kde-frameworks/kdbusaddons:6[>=${KF6_MIN_VER}]
        kde-frameworks/ki18n:6[>=${KF6_MIN_VER}]
        x11-libs/qtbase:6[>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:6[>=${QT_MIN_VER}]
        x11-libs/qtsvg:6[>=${QT_MIN_VER}]
    run:
        kde/kirigami-addons:6[>=1.0]
        kde-frameworks/kirigami:6[>=${KF6_MIN_VER}]
        kde-frameworks/knotifications:6[>=${KF6_MIN_VER}] [[
            note = [ also in cmake, but only used with QML ]
        ]]
"

francis_src_test() {
    export QT_QPA_PLATFORM=vnc:port=0
    cmake_src_test
}

