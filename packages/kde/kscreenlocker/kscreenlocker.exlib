# Copyright 2015-2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
myexparam kf5_min_ver
myexparam qt_min_ver

exparam -v major_version kf_major_version

require plasma kde [ kf_major_version=${major_version} translations='ki18n' ]
require xdummy [ phase=test ] test-dbus-daemon

export_exlib_phases src_test

SUMMARY="Library and components for a secure lock screen architecture"

LICENCES="
    BSD-3 [[ note = [ cmake scripts ] ]]
    GPL-2
"
SLOT="0"
MYOPTIONS="
    ( providers: elogind systemd ) [[
        *description = [ Session tracking provider ]
        number-selected = at-most-one
    ]]
"

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        kde-frameworks/kcmutils:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kglobalaccel:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kidletime:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kio:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kpackage:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/solid:${major_version}[>=${KF5_MIN_VER}]
        sys-libs/pam
        sys-libs/wayland
        x11-libs/libX11
        x11-libs/libxcb
        x11-libs/libXi
        x11-libs/qtbase:${major_version}[>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:${major_version}[>=${QT_MIN_VER}]
        x11-utils/xcb-util-keysyms
       !kde/plasma-workspace:4[<5.4.95] [[
            description = [ kscreenlocker has been split out from plasma-workspace ]
            resolution = uninstall-blocked-after
        ]]
    test:
        sys-libs/pam_wrapper
    post:
        kde/kde-cli-tools:4 [[ note = [ kcmshell5 ] ]]
    recommendation:
        kde/kde-pam [[
            description = [ Required to unlock the screen with some configurations
        ] ]]
        (
            providers:elogind? ( sys-auth/elogind )
            providers:systemd? ( sys-apps/systemd )
        ) [[ *description = [ loginctl for emergency unlocking ] ]]
"

if ever at_least 5.27.80 ; then
    DEPENDENCIES+="
        build+run:
            kde/layer-shell-qt[>=5.27.80]
            kde/libplasma[>=${PV}]
            kde-frameworks/ksvg:${major_version}[>=${KF5_MIN_VER}]
            kde-frameworks/libkscreen:6
    "
else
    DEPENDENCIES+="
        build+run:
            kde/layer-shell-qt
            kde-frameworks/kconfigwidgets:${major_version}[>=${KF5_MIN_VER}]
            kde-frameworks/kdeclarative:5[>=${KF5_MIN_VER}]
            kde-frameworks/kwayland:${major_version}
            kde-frameworks/libkscreen:5
            x11-libs/qtx11extras:5[>=${QT_MIN_VER}]
        run:
            x11-libs/qtquickcontrols:5[>=${QT_MIN_VER}]
    "
fi

CMAKE_SRC_CONFIGURE_PARAMS+=( -DPAM_REQUIRED:BOOL=TRUE )

CMAKE_SRC_TEST_PARAMS+=(
    # - kscreenlocker-killTest: Fails under sydbox
    # - ksmserver-ksldTest, ksld-noScreensTest: Want to access the system dbus
    -E '(kscreenlocker-killTest|ksmserver-ksldTest|ksld-noScreensTest|logindTest|ksmserver-pamTest)'
)

kscreenlocker_src_test() {
    xdummy_start

    test-dbus-daemon_run-tests cmake_src_test

    xdummy_stop
}

