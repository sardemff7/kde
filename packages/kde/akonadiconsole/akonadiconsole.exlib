# Copyright 2016-2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
myexparam kf5_min_ver
myexparam qt_min_ver

exparam -v major_version kf_major_version

require kde-apps kde [ kf_major_version=${major_version} translations=kde ]
require xdummy [ phase=test ] test-dbus-daemon
require gtk-icon-cache

export_exlib_phases src_test

SUMMARY="Akonadi Management and Debugging Console"

LICENCES="FDL-1.2 GPL-2 LGPL-2.1"
SLOT="0"
MYOPTIONS=""

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:${major_version}[>=${KF5_MIN_VER}]
    build+run:
        dev-db/xapian-core
        kde-frameworks/kcalendarcore:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcompletion:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcontacts:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kio:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kitemmodels:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kitemviews:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/ktextwidgets:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:${major_version}[>=${KF5_MIN_VER}]
        x11-libs/qtbase:${major_version}[>=${QT_MIN_VER}][sql]
"

if ever at_least 24.02.0 ; then
    DEPENDENCIES+="
        build+run:
            kde/calendarsupport[>=${PV}]
            kde/libkdepim[>=${PV}]
            kde/messagelib[>=${PV}]
            kde-frameworks/akonadi-contact:5[>=${PV}]
            kde-frameworks/akonadi-search:5[>=${PV}]
            kde-frameworks/kcolorscheme:${major_version}[>=${KF5_MIN_VER}]
            kde-frameworks/kmime:${major_version}[>=${PV}]
            server-pim/akonadi:5[>=${PV}]
    "
else
    DEPENDENCIES+="
        build+run:
            kde/calendarsupport[>=${PV}&<24]
            kde/libkdepim[>=${PV}&<24]
            kde/messagelib[>=${PV}&<24]
            kde-frameworks/akonadi-contact:5[>=${PV}&<24]
            kde-frameworks/akonadi-search:5[>=${PV}&<24]
            kde-frameworks/kmime:5[>=${PV}]
            server-pim/akonadi:5[>=${PV}&<24]
    "
fi

akonadiconsole_src_test() {
    xdummy_start

    test-dbus-daemon_run-tests cmake_src_test

    xdummy_stop
}

