# Copyright 2015, 2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
myexparam qt_min_ver

exparam -v major_version kf_major_version

require kde.org kde [ kf_major_version=${major_version} translations='qt' ]
require xdummy [ phase=test ]

export_exlib_phases src_test

SUMMARY="A library for creating business charts"
DESCRIPTION="
Besides having all the standard features, it enables the developer to design
and manage a large number of axes and provides sophisticated customization.
KD Chart utilizes the Qt Model-View programming model and allows for re-use of
existing data models to create charts. KD Chart is a complete implementation
of the ODF (OpenDocument) Chart specification. It now includes Stock Charts,
Box & Whisker Charts and the KD Gantt module for implementing ODF Gantt charts
into applications.
This is a KDE-fied version of the one offered by KDAB which has no public
releases, see http://lists.kde.org/?l=kde-core-devel&m=142335191017238&w=2
for details."

HOMEPAGE+=" https://www.kdab.com/kdab-products/kd-chart/"

LICENCES="GPL-2"
MYOPTIONS="doc"

exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build:
        x11-libs/qttools:${major_version}
        doc? (
            app-doc/doxygen
            x11-libs/qttools:${major_version}   [[ note = qhelpgenerator ]]
        )
    build+run:
        x11-libs/qtbase:${major_version}[>=${QT_MIN_VER}][sql]
        x11-libs/qtsvg:${major_version}[>=${QT_MIN_VER}]
"

CMAKE_SRC_CONFIGURE_OPTION_BUILDS+=( 'doc QCH' )

kdiagram_src_test() {
    xdummy_start

    cmake_src_test

    xdummy_stop
}

