# Copyright 2013 Kim Højgaard-Hansen <kimrhh@exherbo.org>
# Copyright 2015-2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
myexparam kf5_min_ver
myexparam qt_min_ver

exparam -v major_version kf_major_version

require plasma kde [ kf_major_version=${major_version} ]

SUMMARY="GTK2 and GTK3 Configurator for KDE"
DESCRIPTION="
Configuration dialog to adapt GTK applications appearance to your taste under
KDE. Among its many features, it lets you:
- Choose which theme is used for GTK2 and GTK3 applications.
- Tweak some GTK applications behaviour.
- Select what icon theme to use in GTK applications.
- Select GTK applications default fonts.
- Easily browse and install new GTK2 and GTK3 themes.
"

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

LICENCES="GPL-2 LGPL-2.1"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-lang/sassc
        dev-libs/atk
        dev-libs/glib:2
        gnome-desktop/gsettings-desktop-schemas
        kde-frameworks/kconfig:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kguiaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:${major_version}[>=${KF5_MIN_VER}]
        x11-libs/cairo
        x11-libs/gdk-pixbuf:2.0
        x11-libs/gtk+:3
        x11-libs/libX11
        x11-libs/qtbase:${major_version}[>=${QT_MIN_VER}]
        x11-libs/qtsvg:${major_version}[>=${QT_MIN_VER}]
    recommendation:
        x11-apps/xsettingsd [[ description = [ Apply setttings to GTK apps on the fly ] ]]
"

if ever at_least 5.50.0 ; then
    DEPENDENCIES+="
        build+run:
            kde/kdecoration:4[>=5.50.0]
            kde-frameworks/kcolorscheme:${major_version}[>=${KF5_MIN_VER}]
    "
else
    DEPENDENCIES+="
        build+run:
            kde/kdecoration:4[>=5.22.80]
            kde-frameworks/kconfigwidgets:${major_version}[>=${KF5_MIN_VER}]
    "
fi

