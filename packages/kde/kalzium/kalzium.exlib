# Copyright 2011 Bernd Steinhauser <berniyh@exherbo.org>
# Copyright 2016-2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon kdeedu.exlib, which is:
# Copyright 2008 Bernd Steinhauser <berniyh@exherbo.org>
# Copyright 2008-2011 Bo Ørsted Andresen <zlin@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf5_min_ver
myexparam qt_min_ver

require kde-apps kde [ translations='ki18n' ] freedesktop-desktop gtk-icon-cache

export_exlib_phases pkg_postinst pkg_postrm

SUMMARY="Periodic Table of Elements"

LICENCES="GPL-2 FDL-1.2"
MYOPTIONS="
    3dviewer [[ description = [ Build the molecular editor ] ]]
    solver   [[ description = [ Build the equation solver ] ]]
"

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
        sys-devel/gettext
        virtual/pkg-config
        3dviewer? ( sci-libs/eigen:3[>=3.0.0] )
    build+run:
        kde-frameworks/karchive:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemviews:5[>=${KF5_MIN_VER}]
        kde-frameworks/kplotting:5[>=${KF5_MIN_VER}]
        kde-frameworks/ktextwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kunitconversion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtscript:5[>=${QT_MIN_VER}]
        x11-libs/qtsvg:5[>=${QT_MIN_VER}]
        3dviewer? (
            kde-frameworks/knewstuff:5[>=${KF5_MIN_VER}]
            sci-chemistry/avogadrolibs[qt5]
            sci-chemistry/openbabel:=[>=2.2]
        )
        solver? (
            dev-lang/ocaml
            dev-ml/facile
        )
    suggestion:
        x11-misc/chemical-mime-data [[ description = [ Needed to open CML molecules from the
                file manager ] ]]
"

if ever at_least 24.01.75 ; then
    DEPENDENCIES+="
        build+run:
            kde-frameworks/ktextwidgets:5[>=${KF5_MIN_VER}]
    "
else
    DEPENDENCIES+="
        build+run:
            kde-frameworks/kparts:5[>=${KF5_MIN_VER}]
    "
fi

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    '3dviewer AvogadroLibs'
    '3dviewer Eigen3'
    '3dviewer OpenBabel2'
    'solver OCaml'
    'solver Libfacile'
)

kalzium_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

kalzium_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

