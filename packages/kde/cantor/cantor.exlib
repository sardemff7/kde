# Copyright 2011 Kim Højgaard-Hansen <kimrhh@exherbo.org>
# Copyright 2015-2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf5_min_ver
myexparam qt_min_ver

require kde-apps kde [ translations='ki18n' ] freedesktop-desktop gtk-icon-cache
require xdummy [ phase=test ] test-dbus-daemon
require python [ blacklist='2' with_opt=true multibuild=false ]

export_exlib_phases src_configure src_test pkg_postinst pkg_postrm

SUMMARY="KDE Frontend to Mathematical Software"

LICENCES="GPL-2 FDL-1.2"
MYOPTIONS="
    analitza  [[ description = [ Build the analitza (kalgebra) backend ] ]]
    julia     [[ description = [ Build Julia backend, a programming language for technical computing ] ]]
    lua       [[ description = [ Build the lua scripting backend ] ]]
    qalculate [[ description = [ Build the qalculate backend ] ]]
    R         [[ description = [ Build the R backend ] ]]
    ( analitza lua qalculate R ) [[ number-selected = at-least-one ]]
"

# FIXME: Find out what a module is used for and provide a usable description
# cantor needs one of these backends to be usable:
# Octave: http://www.octave.org/
# Sage: http://www.sagemath.org/
# Maxima: http://maxima.sourceforge.net/

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
        virtual/pkg-config
    build+run:
        app-text/libspectre
        app-text/poppler[>=0.62.0][qt5]
        kde-frameworks/karchive:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/knewstuff:5[>=${KF5_MIN_VER}]
        kde-frameworks/kparts:5[>=${KF5_MIN_VER}]
        kde-frameworks/ktexteditor:5[>=${KF5_MIN_VER}]
        kde-frameworks/ktextwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        kde-frameworks/syntax-highlighting:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtsvg:5[>=${QT_MIN_VER}]
        x11-libs/qttools:5[>=${QT_MIN_VER}]
        x11-libs/qtwebengine:5[>=${QT_MIN_VER}]
        x11-libs/qtxmlpatterns:5[>=${QT_MIN_VER}]
        x11-misc/shared-mime-info[>=1.3]
        analitza? ( kde/analitza:5[<24] )
        julia? ( sci-lang/julia )
        lua? ( dev-lang/LuaJIT )
        R? (
            sci-lang/R[shared]
            sys-libs/libgfortran:=
        )
    suggestion:
        sci-apps/maxima
        sci-apps/octave
        sci-apps/sage
"

if ever at_least 24.01.75 ; then
    DEPENDENCIES+="
        build+run:
            qalculate? ( sci-libs/libqalculate:=[>=3.11] )
    "
else
    DEPENDENCIES+="
        build+run:
            qalculate? ( sci-libs/libqalculate:= )
    "
fi

# Currently bundled because of two patches, which were not accepted upstream
# (yet):
#        markdown  [[ description = [ Write entries using Markdown markup language ] ]]
#        markdown? ( app-text/discount[>=2.2.0] )
#        $(cmake_disable_find markdown Discount)

# Many tests crash possibly because they expect tools, which aren't available
RESTRICT="test"

CMAKE_SRC_TEST_PARAMS+=(
    # - testlua
    # - testworksheet wants to access the system dbus
    -E '(testr|testlua|testworksheet)'
)

cantor_src_configure() {
    configure_one_multibuild
}

configure_one_multibuild() {
    local cmakeparams=(
        -DPython3_EXECUTABLE:PATH=${PYTHON}
        -DUSE_LIBSPECTRE:BOOL=TRUE
        $(cmake_disable_find analitza Analitza5)
        $(cmake_disable_find Julia)
        $(cmake_disable_find lua LuaJIT)
        $(cmake_disable_find python Python3)
        $(cmake_disable_find Qalculate)
        $(cmake_disable_find R R)
    )

    ecmake \
        $(kf5_shared_cmake_params) \
        "${CMAKE_SRC_CONFIGURE_TESTS[@]}" \
        "${cmakeparams[@]}"
}

cantor_src_test() {
    xdummy_start

    test-dbus-daemon_run-tests cmake_src_test

    xdummy_stop
}

cantor_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

cantor_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

