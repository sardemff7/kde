# Copyright 2015-2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
myexparam kf5_min_ver
myexparam qt_min_ver

exparam -v major_version kf_major_version

require kde-apps kde [ kf_major_version=${major_version} translations='ki18n' ] test-dbus-daemon

SUMMARY="Small system to administer web accounts for sites and services"

LICENCES="GPL-2 LGPL-2.1"
MYOPTIONS=""

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        kde-frameworks/kcmutils:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kdeclarative:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kio:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kwallet:${major_version}[>=${KF5_MIN_VER}]
        x11-libs/qtbase:${major_version}[>=5.7.0]
        x11-libs/qtdeclarative:${major_version}[>=5.7.0]
    run:
        dev-util/intltool [[ note = [ intltool-merge in kaccounts_add_* cmake macros ] ]]
    post:
        kde/kaccounts-providers:4 [[ note = [ Unbreak a cycle with this package ] ]]
    recommendation:
        net-libs/signon-plugin-oauth2 [[
            description = [ Required for the login process of some online accounts, e.g. Google ]
        ]]
"

if ever at_least 24.01.85 ; then
    DEPENDENCIES+="
        build+run:
            dev-libs/qcoro[providers:qt6]
            net-libs/accounts-qt[>=1.17]
            net-libs/signon[>=8.56]
        recommendation:
            net-apps/signon-ui[>=scm] [[
                description = [ Required for the login process of some online accounts, e.g. Google ]
            ]]
    "

    CMAKE_SRC_CONFIGURE_PARAMS+=(
        # "Enable this to get a build to support KF5 applications"
        # TODO: We probably need this for libkgapi, but that would probably
        # mean to make accounts-qt/signon co-install for multiple Qt versions
        -DKF6_COMPAT_BUILD:BOOL=FALSE
    )
else
    DEPENDENCIES+="
        build+run:
            dev-libs/qcoro[providers:qt5]
            net-libs/accounts-qt[>=1.16&<1.17]
            net-libs/signon[>=8.55&<8.62]
            x11-libs/qtquickcontrols:5[>=5.2.0]
        run:
            kde/kde-cli-tools:4 [[ note = [ kcmshell5 kcm_kaccounts ] ]]
        recommendation:
            kde/signon-kwallet-extension:4 [[
                description = [ Support for storing the login credentials of online accounts in KWallet ]
            ]]
            net-apps/signon-ui[<scm] [[
                description = [ Required for the login process of some online accounts, e.g. Google ]
            ]]
    "
fi

