# Copyright 2016-2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
myexparam kf5_min_ver
myexparam qt_min_ver

exparam -v major_version kf_major_version

require kde-apps kde [ kf_major_version=${major_version} translations='ki18n' ]
require xdummy [ phase=test ] test-dbus-daemon
require freedesktop-desktop gtk-icon-cache

export_exlib_phases src_test pkg_postinst pkg_postrm

SUMMARY="Certificate manager and GUI for OpenPGP and CMS cryptography"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS=""

if ever at_least 24.02.0 ; then
    GPGME_MIN_VER="1.20.0"
else
    GPGME_MIN_VER="1.16.0"
fi
exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:${major_version}[>=${KF5_MIN_VER}]
        virtual/pkg-config
    build+run:
        app-crypt/gpgme[>=${GPGME_MIN_VER}]
        dev-libs/libassuan:=[>=2.4.2]
        dev-libs/libgpg-error[>=1.36]
        kde-frameworks/kcodecs:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kio:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kitemmodels:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:${major_version}[>=${KF5_MIN_VER}]   [[ note = [ DISABLE_KWATCHGNUPG ] ]]
        kde-frameworks/ktextwidgets:${major_version}[>=${KF5_MIN_VER}]   [[ note = [ DISABLE_KWATCHGNUPG ] ]]
        kde-frameworks/kwidgetsaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:${major_version}[>=${KF5_MIN_VER}]
        x11-libs/qtbase:${major_version}[>=${QT_MIN_VER}][gui]
        x11-misc/shared-mime-info[>=1.3]
        !kde-frameworks/gpgmepp:${major_version} [[
            description = [ gpgme causes a build failure due to similar include paths ]
            resolution = uninstall-blocked-before
        ]]
"

if ever at_least 24.02.2 ; then
    DEPENDENCIES+="
        build+run:
            app-crypt/libqgpgme[>=${GPGME_MIN_VER}][providers:qt6]
            kde/libkleo[>=${PV}]
            kde/mimetreeparser[>=${PV}]
            kde-frameworks/akonadi-mime:5[>=${PV}]
            kde-frameworks/kcolorscheme:${major_version}[>=${KF5_MIN_VER}]
            kde-frameworks/kidentitymanagement:${major_version}[>=${PV}]
            kde-frameworks/kmime:${major_version}[>=${PV}]
            kde-frameworks/kmailtransport:5[>=${PV}]
            kde-frameworks/kstatusnotifieritem:${major_version}[>=${KF5_MIN_VER}]
    "
else
    DEPENDENCIES+="
        build+run:
            app-crypt/libqgpgme[>=${GPGME_MIN_VER}][providers:qt5]
            kde/libkleo[>=${PV}&<24]
            kde-frameworks/akonadi-mime:5[>=${PV}&<24]
            kde-frameworks/kcmutils:5[>=${KF5_MIN_VER}]   [[ note = [ FORCE_DISABLE_KCMUTILS ] ]]
            kde-frameworks/kidentitymanagement:5[>=${PV}]
            kde-frameworks/kmailtransport:5[>=${PV}&<24]
            kde-frameworks/kmime:5[>=${PV}]
    "
fi

kleopatra_src_test() {
    xdummy_start

    test-dbus-daemon_run-tests cmake_src_test

    xdummy_stop
}

kleopatra_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

kleopatra_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

