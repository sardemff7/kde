# Copyright 2015-2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf5_min_ver
myexparam qt_min_ver

require plasma kde
require test-dbus-daemon

SUMMARY="Integration plugins for KDE frameworks for the wayland windowing system (Qt5)"

LICENCES="LGPL-2.1"
SLOT="4"
MYOPTIONS=""

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build:
        sys-libs/wayland-protocols[>=1.21]
        virtual/pkg-config
    build+run:
        kde-frameworks/kwayland:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        sys-libs/wayland[>=1.15]
        x11-libs/libxkbcommon
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtwayland:5[>=${QT_MIN_VER}]
"

if ever at_least 5.92.0 ; then
    DEPENDENCIES+="
        build:
            kde/plasma-wayland-protocols
    "
else
    DEPENDENCIES+="
        build:
            kde-frameworks/kidletime:5[>=${KF5_MIN_VER}]
    "
fi

# The one meaningful test (besides the appstream test) fails (5.14.0)
RESTRICT="test"

