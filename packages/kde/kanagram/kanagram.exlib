# Copyright 2011 Bernd Steinhauser <berniyh@exherbo.org>
# copyright 2014-2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
myexparam kf5_min_ver
myexparam qt_min_ver

exparam -v major_version kf_major_version

require kde-apps kde [ kf_major_version=${major_version} translations='ki18n' ]
require gtk-icon-cache

SUMMARY="Puzzle based on anagrams of words"
DESCRIPTION="
The puzzle is solved when the letters of the scrambled word are put back in the correct order.
There is no limit on either time taken, or the amount of attempts to solve the word. Included
with Kanagram are several vocabularies ready to play, with many more available from the Internet.
A scoring system has been added recently that can be customized from the settings."

LICENCES="GPL-2 FDL-1.2"
MYOPTIONS+=" tts [[ description = [ Support for text to speech ] ]]"

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:${major_version}[>=${KF5_MIN_VER}]
    build+run:
        kde-frameworks/kconfig:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kio:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/knewstuff:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/sonnet:${major_version}[>=${KF5_MIN_VER}]
        x11-libs/qtbase:${major_version}[>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:${major_version}[>=${QT_MIN_VER}]
        tts? ( x11-libs/qtspeech:${major_version}[>=${QT_MIN_VER}] )
    run:
        kde/kdeedu-data:${SLOT}
        x11-libs/qtmultimedia:${major_version}[>=${QT_MIN_VER}] [[ note = [ import QtMultimedia 5.0 ] ]]
"

if ever at_least 24.01.85 ; then
    DEPENDENCIES+="
        build+run:
            kde/libkeduvocdocument:${SLOT}[>=24.01.85]
    "
else
    DEPENDENCIES+="
        build+run:
            kde/libkeduvocdocument:${SLOT}[<24]
            x11-libs/qtquickcontrols:${major_version}[>=${QT_MIN_VER}]
    "
fi

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    "tts Qt${major_version}TextToSpeech"
)

