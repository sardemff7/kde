# Copyright 2023 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
myexparam kf5_min_ver
myexparam qt_min_ver

exparam -v major_version kf_major_version

require kde-apps [ group_or_user=graphics ] kde [ kf_major_version=${major_version} translations=ki18n ]
require freedesktop-desktop gtk-icon-cache
require xdummy [ phase=test ]

export_exlib_phases src_test pkg_postinst pkg_postrm

SUMMARY="EPub Reader for mobile devices"

LICENCES="
    BSD-2 [[ note = [ cmake scripts ] ]]
    BSD-3 [[ note = [ epubcontainer ] ]]
    CC0   [[ note = [ dot files, appstream, desktop files, etc ] ]]
    || ( LGPL-2.1 LGPL-3.0 )
    || ( GPL-2 GPL-3 )
    GPL-3
    MIT   [[ note = [ epub.js ] ]]
"
SLOT="0"
MYOPTIONS=""

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build+run:
        kde-frameworks/baloo:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/karchive:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kfilemetadata:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kquickcharts:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:${major_version}[>=${KF5_MIN_VER}]
        x11-libs/qtbase:${major_version}[>=${QT_MIN_VER}][sql]
        x11-libs/qtdeclarative:${major_version}[>=${QT_MIN_VER}]
        x11-libs/qtsvg:${major_version}[>=${QT_MIN_VER}]
        x11-libs/qtwebsockets:${major_version}[>=${QT_MIN_VER}]
        x11-libs/qtwebengine:${major_version}[>=${QT_MIN_VER}]
    run:
"

if ever at_least 24.01.75 ; then
    DEPENDENCIES+="
        build+run:
            x11-libs/qthttpserver:${major_version}[>=${QT_MIN_VER}]
            x11-libs/qtwebchannel:${major_version}[>=${QT_MIN_VER}][qml]
        run:
            kde/kirigami-addons:${major_version}[>=0.11]
            kde-frameworks/kirigami:${major_version}[>=${KF5_MIN_VER}]
            kde-frameworks/qqc2-desktop-style:${major_version}[>=${KF5_MIN_VER}]
    "
else
    DEPENDENCIES+="
        build+run:
            x11-libs/qtquickcontrols2:5[>=${QT_MIN_VER}]
            x11-libs/qtwebchannel:5[>=${QT_MIN_VER}]
        run:
            kde/kirigami-addons:0[>=0.10]
            kde-frameworks/qqc2-desktop-style:0[>=${KF5_MIN_VER}]
            x11-libs/qtgraphicaleffects:5[>=${QT_MIN_VER}]
    "
fi

arianna_src_test() {
    xdummy_start

    cmake_src_test

    xdummy_stop
}

arianna_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

arianna_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

