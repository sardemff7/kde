# Copyright 2013, 2015-2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
myexparam kf5_min_ver
myexparam qt_min_ver

exparam -v major_version kf_major_version

require kde-apps kde [ kf_major_version=${major_version} translations='ki18n' ]
require freedesktop-desktop gtk-icon-cache

export_exlib_phases pkg_postinst pkg_postrm

SUMMARY="Battleship Game"
DESCRIPTION="
Ships are placed on a board which represents the sea. Players try to hit each others ships in turns
without knowing where they are placed. The first player to destroy all ships wins the game."
HOMEPAGE+=" https://apps.kde.org/${PN}/"

LICENCES="GPL-2 FDL-1.2"
MYOPTIONS=""

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:${major_version}[>=${KF5_MIN_VER}]
    build+run:
        kde-frameworks/kcompletion:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kdnssd:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/ktextwidgets:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:${major_version}[>=${KF5_MIN_VER}]
        x11-libs/qtbase:${major_version}[>=${QT_MIN_VER}][gui]
"

if ever at_least 24.04.80 ; then
    CMAKE_SRC_CONFIGURE_PARAMS+=(
        # TODO: Add app-arch/p7zip but the 7zip versions are confusing and
        # Find7Zip module doesn't work with our p7zip yet; falls back to gzip
        -DCMAKE_DISABLE_FIND_PACKAGE_7Zip:BOOL=TRUE
    )
fi

if ever at_least 24.01.75 ; then
    DEPENDENCIES+="
        build+run:
            kde/libkdegames:5[>=24.01.75]
            kde-frameworks/kwidgetsaddons:${major_version}[>=${KF5_MIN_VER}]
    "
else
    DEPENDENCIES+="
        build+run:
            kde/libkdegames:5[>=4.9.0&<24]
    "
fi

knavalbattle_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

knavalbattle_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}
