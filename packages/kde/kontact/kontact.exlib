# Copyright 2016-2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
myexparam kf5_min_ver
myexparam qt_min_ver

exparam -v major_version kf_major_version

require kde-apps kde [ kf_major_version=${major_version} translations='ki18n' ]
require gtk-icon-cache

SUMMARY="An integrated solution to your personal information management (PIM) needs"
DESCRIPTION="
It combines well-known KDE applications like KMail, KOrganizer and KAddressBook
into a single interface to provide easy access to mail, scheduling, address
book and other PIM functionality."

LICENCES="FDL-1.2 GPL-2 LGPL-2.1"
SLOT="0"
MYOPTIONS=""

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:${major_version}[>=${KF5_MIN_VER}]
    build+run:
        kde-frameworks/kcmutils:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kguiaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kio:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kpimtextedit:${major_version}[>=${PV}]
        x11-libs/qtbase:${major_version}[>=${QT_MIN_VER}][gui]
        x11-libs/qtwebengine:${major_version}[>=${QT_MIN_VER}]
"

if ever at_least 24.02.0 ; then
    DEPENDENCIES+="
        build+run:
            kde/grantleetheme:6[>=${PV}]
            kde/libkdepim[>=${PV}]
            kde/pimcommon[>=${PV}]
            kde-frameworks/kcolorscheme:${major_version}[>=${KF5_MIN_VER}]
            kde-frameworks/kontactinterface:${major_version}[>=${PV}]
    "
else
    DEPENDENCIES+="
        build+run:
            kde/grantleetheme:0[>=${PV}]
            kde/libkdepim[>=${PV}&<24]
            kde/pimcommon[>=${PV}&<24]
            kde-frameworks/kontactinterface:5[>=${PV}]
    "
fi

