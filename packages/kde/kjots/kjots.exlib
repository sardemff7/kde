# Copyright 2023 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf6_min_ver
myexparam qt_min_ver

require kde.org
require kde [ kf_major_version=6 translations=ki18n ]
require gtk-icon-cache

SUMMARY="Note taking application using Akonadi"

LICENCES="
    GPL-2
    LGPL-2.0
"
SLOT="0"
MYOPTIONS="
    tts [[ description = [  Support for text to speech ] ]]
"

exparam -v KF6_MIN_VER kf6_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build+run:
        kde/ktextaddons:6[>=1.5.0]
        kde-frameworks/akonadi-notes:5[>=24.02.0]
        kde-frameworks/kbookmarks:6[>=${KF6_MIN_VER}]
        kde-frameworks/kcmutils:6[>=${KF6_MIN_VER}]
        kde-frameworks/kconfig:6[>=${KF6_MIN_VER}]
        kde-frameworks/ki18n:6[>=${KF6_MIN_VER}]
        kde-frameworks/kio:6[>=${KF6_MIN_VER}]
        kde-frameworks/kmime:6[>=24.02.0]
        kde-frameworks/kontactinterface:6[>=24.02.0]
        kde-frameworks/kparts:6[>=${KF6_MIN_VER}]
        kde-frameworks/kpimtextedit:6[>=24.02.0]
        kde-frameworks/ktexttemplate:6[>=${KF6_MIN_VER}]
        kde-frameworks/kxmlgui:6[>=${KF6_MIN_VER}]
        server-pim/akonadi:5[>=24.02.0]
        x11-libs/qtbase:6[>=${QT_MIN_VER}]
        tts? ( kde/ktextaddons:6[>=1.5.0][tts] )
"

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    "tts KF6TextEditTextToSpeech"
)

