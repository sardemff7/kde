# Copyright 2011 Bernd Steinhauser <berniyh@exherbo.org>
# Copyright 2014-2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
myexparam kf5_min_ver
myexparam qt_min_ver

exparam -v major_version kf_major_version

require kde-apps kde [ kf_major_version=${major_version} translations='ki18n' ]
require gtk-icon-cache

SUMMARY="A game based on the well-known hangman game"
DESCRIPTION="
It is aimed at children aged six and over. The game has several categories of words to play with,
for example: Animals (animals words) and three difficulty categories: Easy, Medium and Hard. A
word is picked at random, the letters are hidden, and you must guess the word by trying one
letter after another. Each time you guess a wrong letter, part of a picture of a hangman is drawn.
You must guess the word before being hanged! You have 10 tries."

LICENCES="GPL-2 FDL-1.2"
MYOPTIONS=""

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:${major_version}[>=${KF5_MIN_VER}]
    build+run:
        kde-frameworks/kcompletion:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kio:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/knewstuff:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:${major_version}[>=${KF5_MIN_VER}]
        x11-libs/qtbase:${major_version}[>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:${major_version}[>=${QT_MIN_VER}]
        x11-libs/qtsvg:${major_version}[>=${QT_MIN_VER}]
    run:
        kde/kdeedu-data:${SLOT}
        x11-libs/qtmultimedia:${major_version}[>=${QT_MIN_VER}] [[ note = [ import QtMultimedia 5.0 ] ]]
"

if ever at_least 24.01.85 ; then
    DEPENDENCIES+="
        build+run:
            kde/libkeduvocdocument:${SLOT}[>=24.01.85]
        run:
            x11-libs/qt5compat:6[>=${QT_MIN_VER}] [[ note = [ Qt5Compat.GraphicalEffects ] ]]
    "
else
    DEPENDENCIES+="
        build+run:
            kde/libkeduvocdocument:${SLOT}[<24]
            kde-frameworks/kxmlgui:${major_version}[>=${KF5_MIN_VER}]
        run:
            x11-libs/qtgraphicaleffects:${major_version}[>=${QT_MIN_VER}]
            x11-libs/qtquickcontrols:${major_version}[>=${QT_MIN_VER}]
    "
fi

