# Copyright 2018-2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
myexparam kf5_min_ver
myexparam qt_min_ver

exparam -v major_version kf_major_version

require kde-apps kde [ kf_major_version=${major_version} translations='ki18n' ]
require freedesktop-mime
require flag-o-matic
require alternatives

export_exlib_phases pkg_setup src_test src_install

SUMMARY="A library containing a itinerary data model and itinerary extraction code"
DESCRIPTION="
This follows the reservation ontology from https://schema.org and Google's
extensions to it (https://developers.google.com/gmail/markup/reference/).
"

LICENCES="LGPL-2.1"
SLOT="0"
MYOPTIONS="
    doc [[ description = [ Build API docs in Qt's help format (.qch) ] ]]
    phone-numbers [[ description = [ Parsing and geo-coding of phone numbers ] ]]

    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build:
        doc? (
            app-doc/doxygen
            x11-libs/qttools:${major_version}   [[ note = qhelpgenerator ]]
        )
    build+run:
        app-text/poppler
        dev-libs/libxml2:2.0
        kde-frameworks/karchive:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcalendarcore:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcodecs:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcontacts:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:${major_version}[>=${KF5_MIN_VER}]
        media-libs/zxing-cpp:=
        sys-libs/zlib
        x11-libs/qtbase:${major_version}[>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:${major_version}[>=${QT_MIN_VER}]
        x11-misc/shared-mime-info[>=1.3]
        phone-numbers? ( dev-libs/libphonenumber )
        providers:openssl? ( dev-libs/openssl:=[>=1.1] )
        providers:libressl? ( dev-libs/libressl:= )
"

if ever at_least 24.02.0 ; then
    DEPENDENCIES+="
        build+run:
            kde/kpkpass:${major_version}[>=${PV}]
            kde-frameworks/kmime:${major_version}[>=${PV}]
    "
else
    DEPENDENCIES+="
        build+run:
            kde/kpkpass:0[>=${PV}]
            kde-frameworks/kmime:5[>=${PV}]
    "
fi

CMAKE_SRC_CONFIGURE_PARAMS+=(
    # "Needed only for regenereating the airport database (ie. you most
    # likely don't need this)"
    -DCMAKE_DISABLE_FIND_PACKAGE_OsmTools:BOOL=TRUE
)

CMAKE_SRC_CONFIGURE_OPTION_BUILDS+=( 'doc QCH' )

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'phone-numbers PhoneNumber'
)

CMAKE_SRC_TEST_PARAMS+=(
    # They seem to fail due to some locale trouble
    -E '(structureddataextractortest|htmldocumenttest|extractortest)'
)

kitinerary_pkg_setup() {
    if option providers:libressl ; then
        # Fixes an build error with LibreSSL and ld.bfd, cf.
        # https://bugreports.qt.io/browse/QTBUG-63291
        append-ldflags -Wl,--no-fatal-warnings
    fi
}

kitinerary_src_test() {
    export QT_QPA_PLATFORM=vnc:port=0

    cmake_src_test
}

kitinerary_src_install() {
    kde_src_install

    alternatives_for _${PN} ${SLOT} ${SLOT} \
        /usr/share/mime/packages/application-vnd-kde-itinerary{,-${SLOT}}.xml
}

