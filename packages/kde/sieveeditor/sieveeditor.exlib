# Copyright 2016 Niels Ole Salscheider <olesalscheider@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
myexparam kf5_min_ver
myexparam qt_min_ver

exparam -v major_version kf_major_version

require kde-apps [ pn='pim-sieve-editor' ]
require kde [ kf_major_version=${major_version} translations='ki18n' ]
require xdummy [ phase=test ]

export_exlib_phases src_test

SUMMARY="KDE application to edit sieve scripts"

LICENCES="FDL-1.2 GPL-2 LGPL-2.1"
SLOT="0"
MYOPTIONS+="
    userfeedback [[
        description = [ Allows sending anonymized usage information to KDE ]
    ]]
"

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:${major_version}[>=${KF5_MIN_VER}]
    build+run:
        kde-frameworks/kbookmarks:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kio:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kwallet:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:${major_version}[>=${KF5_MIN_VER}]
        x11-libs/qtbase:${major_version}[>=${QT_MIN_VER}][gui]
"

if ever at_least 24.02.0 ; then
    DEPENDENCIES+="
        build+run:
            sys-auth/qtkeychain[>=0.14.2][providers:qt6]
    "
fi

if ever at_least 24.02.0 ; then
    DEPENDENCIES+="
        build+run:
            kde/libksieve[>=${PV}]
            kde/pimcommon[>=${PV}]
            kde-frameworks/kcolorscheme:${major_version}[>=${KF5_MIN_VER}]
            kde-frameworks/kimap:${major_version}[>=${PV}]
            kde-frameworks/kmailtransport:5[>=${PV}]
            sys-auth/qtkeychain[providers:qt6]
            userfeedback? ( kde-frameworks/kuserfeedback:${major_version}[>=${KF5_MIN_VER}] )
        suggestion:
            kde/kdepim-addons:4[>=24.02.0] [[
                description = [ Provides a plugin for IMAP folder completion ]
            ]]
    "

    CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
        'userfeedback KF6UserFeedback'
    )
else
    DEPENDENCIES+="
        build+run:
            kde/libksieve[>=${PV}&<24]
            kde/pimcommon[>=${PV}&<24]
            kde-frameworks/kimap:5[>=${PV}]
            kde-frameworks/kmailtransport:5[>=${PV}&<24]
            kde-frameworks/kpimtextedit:5[>=${PV}]
            sys-auth/qtkeychain[providers:qt5]
            userfeedback? ( kde/kuserfeedback[>=1.0.0] )
        suggestion:
            kde/kdepim-addons:4[<24] [[
                description = [ Provides a plugin for IMAP folder completion ]
            ]]
    "

    CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
        'userfeedback KUserFeedback'
    )
fi


sieveeditor_src_test() {
    xdummy_start

    cmake_src_test

    xdummy_stop
}

