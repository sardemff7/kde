# Copyright 2011, 2014-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'kdeutils.exlib', which is:
#     Copyright 2008-2011 Bo Ørsted Andresen

myexparam kf_major_version=5
myexparam kf5_min_ver
myexparam qt_min_ver

exparam -v major_version kf_major_version

require kde-apps kde [ kf_major_version=${major_version} translations='ki18n' ]
require freedesktop-desktop gtk-icon-cache
require xdummy [ phase=test ] test-dbus-daemon

export_exlib_phases src_test pkg_postinst pkg_postrm

SUMMARY="Ark is KDE's file archiver"
DESCRIPTION="
Ark is a program for managing various archive formats within the KDE
environment. Archives can be viewed, extracted, created and modified
from within Ark. The program can handle various formats such as tar,
gzip, bzip2, zip, rar and lha (if appropriate command-line programs
are installed). Ark can work closely with Konqueror in the KDE
environment to handle archives, if you install the Konqueror
Integration plugin available in the kdeaddons package.
"
HOMEPAGE+=" https://utils.kde.org/projects/${PN}/"

LICENCES="
    BSD-3 [[ note = [ cmake scripts ] ]]
    GPL-2 LGPL-3 [[ note = [ icons ] ]]"
MYOPTIONS=""

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:${major_version}[>=${KF5_MIN_VER}]
        virtual/pkg-config   [[ note = [ FindLibZip.cmake ] ]]
    build+run:
        app-arch/libarchive[>=3.3.3][zstd]
        app-arch/libzip[>=1.3.0]
        kde-frameworks/kconfig:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kio:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kparts:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kpty:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kservice:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:${major_version}[>=${KF5_MIN_VER}]
        sys-libs/zlib
        x11-libs/qtbase:${major_version}[>=${QT_MIN_VER}][gui]
    suggestion:
        app-arch/arj [[ description = [ Support for the ARJ archive format ] ]]
        app-arch/lzop [[
            description = [ Support unpacking tar.lzo archives in ark ]
            note = [ Enable lzo support of libarchive >= 3.3 would also work ]
        ]]
        app-arch/p7zip [[ description = [ Support packing and unpacking .7z files in ark ] ]]
        app-arch/unrar [[ description = [ Support unpacking .rar files in ark ] ]]
        app-arch/unzip [[ description = [ Support unpacking .zip files in ark ] ]]
        app-arch/zip   [[ description = [ Support creating or editing .zip files in ark ] ]]
"

if ever at_least 24.04.80 ; then
    DEPENDENCIES+="
        run:
            kde-frameworks/breeze-icons:5[>=${KF5_MIN_VER}]
    "
fi

if ever at_least 24.01.90 ; then
    DEPENDENCIES+="
        build+run:
            kde-frameworks/kconfigwidgets:${major_version}[>=${KF5_MIN_VER}]
            kde-frameworks/kfilemetadata:${major_version}[>=${KF5_MIN_VER}]
    "
fi

# TODO: Not entirely sure if it's missing support for some archive types or
# incompatible versions, but too many tests fail
RESTRICT="test"

# - adddialogtest wants to start an ioslave
# - kerfuffle-loadtest and kerfuffle-extracttest don't seem to handle missing
# support  for some archive types gracefully
CMAKE_SRC_TEST_PARAMS+=(
    -E '(adddialogtest|kerfuffle-loadtest|kerfuffle-extracttest)'
)

ark_src_test() {
    xdummy_start

    test-dbus-daemon_run-tests cmake_src_test

    xdummy_stop
}

ark_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

ark_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

