# Copyright 2023 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf6_min_ver
myexparam qt_min_ver

require plasma kde [ kf_major_version=6 translations=ki18n ]

SUMMARY="Support components for porting from KF5/Qt5 to KF6/Qt6"
DESCRIPTION="
Dataengine support is not in the kf6 version of Plasma anymore. Dataengines
should be migrated to QML imports, but in the meantime remaining engines can
use this dataengine implementation."

LICENCES="
    CC0    [[ note = [ .{gitlab,kde}-ci.yml ] ]]
    GPL-2  [[ note = [ serviceoperationstatus.h ] ]]
    LGPL-2
"
SLOT="0"
MYOPTIONS="
    doc [[ description = [ Build API docs in Qt's help format (.qch) ] ]]
"

exparam -v KF6_MIN_VER kf6_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build:
        doc? (
            app-doc/doxygen
            x11-libs/qttools:5   [[ note = qhelpgenerator ]]
        )
    build+run:
        kde-frameworks/kdoctools:6[>=${KF6_MIN_VER}]
        kde-frameworks/kconfig:6[>=${KF6_MIN_VER}]
        kde-frameworks/kcoreaddons:6[>=${KF6_MIN_VER}]
        kde-frameworks/ki18n:6[>=${KF6_MIN_VER}]
        x11-libs/qtbase:6[>=${QT_MIN_VER}][sql]
        x11-libs/qtdeclarative:6[>=${QT_MIN_VER}]
"

if ever at_least 6.0.90 ; then
    DEPENDENCIES+="
        build+run:
            kde/libksysguard
            kde/libplasma[>=$(ever range -3)]
            kde-frameworks/kguiaddons:6[>=${KF6_MIN_VER}]
            kde-frameworks/kio:6[>=${KF6_MIN_VER}]
            kde-frameworks/knotifications:6[>=${KF6_MIN_VER}]
            kde-frameworks/kservice:6[>=${KF6_MIN_VER}]
            kde-frameworks/solid:6[>=${KF6_MIN_VER}]
    "
fi

# Fails, maybe because it fails to load a plugin from the build dir
RESTRICT="test"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DBUILD_COVERAGE:BOOL=FALSE
)
CMAKE_SRC_CONFIGURE_OPTION_BUILDS+=( 'doc QCH' )

