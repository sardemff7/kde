# Copyright 2009 Yury G. Kudryashov <urkud@ya.ru>
# Distributed under the terms of the GNU General Public License v2

myexparam kf5_min_ver
myexparam qt_min_ver

require kde-apps kde [ translations=ki18n ]
require freedesktop-desktop freedesktop-mime gtk-icon-cache
require bash-completion

export_exlib_phases src_prepare src_install pkg_postinst pkg_postrm

SUMMARY="A cross-platform IDE for C, C++, Python, QML/JavaScript and PHP"
DESCRIPTION="
A free, opensource IDE (Integrated Development Environment) for MS Windows, Mac OsX, Linux, Solaris
and FreeBSD. It is a feature-full, plugin extendable IDE for C/C++ and other programing languages.
It is based on KDevPlatform, KDE and Qt libraries and is under development since 1998."
HOMEPAGE="https://www.kdevelop.org/"

LICENCES="
    BSD-3 [[ note = [ cmake scripts ] ]]
    GPL-2 LGPL-2 FDL-1.2
"
SLOT="4"

MYOPTIONS="
    attach-to-process [[ description = [ Easily attach gdb or heaptrack to running processes ] ]]
    okteta [[ description = [ Build okteta hex editor plugin ] ]]
    patch-sharing [[ description = [ Support for sharing patches with kde-frameworks/purpose ] ]]
    plasma [[ description = [ Enables some KDevelop related plasmoids and runners ] ]]
    qmake  [[ description = [ Build the QMake Builder/Manager plugin ] ]]
    subversion [[ description = [ Support for Subversion integration ] ]]
"

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build:
        dev-libs/boost[>=1.35.0]
        sys-devel/gettext
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
        virtual/pkg-config
    build+run:
        dev-lang/clang:17[>=6.0] [[ note = [ There's also a legacy C++ backend ] ]]
        dev-lang/llvm:=
        dev-libs/grantlee:5
        kde/libkomparediff2:4[>=15.03.80]
        kde-frameworks/karchive:5[>=${KF5_MIN_VER}]
        kde-frameworks/kbookmarks:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcmutils:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}] [[ note = [ kcoreaddons_desktop_to_json ] ]]
        kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdeclarative:5[>=${KF5_MIN_VER}]
        kde-frameworks/kguiaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemmodels:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemviews:5[>=${KF5_MIN_VER}]
        kde-frameworks/kjobwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/knewstuff:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifyconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kparts:5[>=${KF5_MIN_VER}]
        kde-frameworks/kservice:5[>=${KF5_MIN_VER}]
        kde-frameworks/ktexteditor:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        kde-frameworks/sonnet:5[>=${KF5_MIN_VER}]
        kde-frameworks/threadweaver:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}][gui]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
        x11-libs/qttools:5[>=${QT_MIN_VER}]   [[ note = [ QtHelp, qdbus ] ]]
        x11-libs/qtwebengine:5[>=${QT_MIN_VER}]
        x11-misc/shared-mime-info[>=1.9]
        attach-to-process? ( kde/libksysguard:4[<5.27.80] )
        okteta? ( kde/okteta:${SLOT}[>=0.26.0] [[ note = [ aka 0.4.0 ] ]] )
        patch-sharing? ( kde-frameworks/purpose:5 )
        plasma? (
            kde-frameworks/krunner:5
            kde-frameworks/plasma-framework:5
        )
        qmake? ( kde/kdevelop-pg-qt[>=1.90.90] )
        subversion? ( dev-scm/subversion[>=1.3.0] )
        !kde/kdevplatform:4 [[
            description = [ kde/kdevplatform:4 was merged into kdevelop ]
            resolution = uninstall-blocked-after
        ]]
    run:
        kde/kate:${SLOT}[>=14.12.0]
    suggestion:
        dev-scm/git [[ description = [ Use Git from within KDevelop ] ]]
        dev-scm/subversion [[ description = [ Use Subversion from within KDevelop ] ]]
        dev-util/clazy [[ description = [ Plugin to check and analyze code with Clazy ] ]]
        dev-util/cppcheck [[ description = [ Plugin for static C/C++ code anylsis ] ]]
        dev-util/heaptrack [[ description = [ Plugin for heap memory profiling ] ]]
        kde/kdev-php [[ description = [ KDevelop plugin for PHP development ] ]]
        kde/kdev-python:4 [[ description = [ KDevelop plugin for Python 3 development ] ]]
        sys-devel/gdb[>=7.0][python] [[ description = [ The only supported debugger ] ]]
        sys-devel/meson [[ description = [ Required by the Meson project manager plugin ] ]]
        sys-devel/ninja [[ description = [ Use ninja to build your projects ] ]]
    test:
        dev-scm/git
"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DBUILD_BENCHMARKS:BOOL=FALSE
    # Can't be found anyway because our current astyle package doesn't build a
    # library, and if one instructs it to build one it doesn't build the
    # executable anymore. kdevelop builds its own copy then.
    -DCMAKE_DISABLE_FIND_PACKAGE_LibAStyle:BOOL=TRUE
    # Should probably use llvm.exlib once that lands
    -DClang_DIR=/usr/$(exhost --target)/lib/llvm/17/lib/cmake/clang
)
CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    "attach-to-process KSysGuard"
    "attach-to-process KF5SysGuard"
    "okteta KastenControllers"
    'okteta OktetaGui'
    "okteta OktetaKastenControllers"
    'patch-sharing KF5Purpose'
    'patch-sharing KDEExperimentalPurpose'
    'plasma KF5Plasma'
    'plasma KF5Runner'
    'qmake KDevelop-PG-Qt'
    'subversion SubversionLibrary'
)

# tests fail due dbus and X11, test-dbus-daemon did not help.
RESTRICT="test"

kdevelop_src_prepare() {
    kde_src_prepare

    # Disable tests needing a running X server
    edo sed -e "/kdevcmake_add_test(cmakeloadprojecttest/d" \
            -e "/kdevcmake_add_test(cmakemanagertest/d" \
            -i plugins/cmake/tests/CMakeLists.txt
    edo sed -e "/add_subdirectory( tests )/d" \
            -i plugins/custom-buildsystem/CMakeLists.txt
}

kdevelop_src_install() {
    kde_src_install

    # Don't install the bash completion into weird dirs...
    nonfatal edo rm -r "${IMAGE}"/usr/$(exhost --target)/usr

    # ...but let dobashcompletion do it.
    dobashcompletion "${CMAKE_SOURCE}"/share/bash-completions/kdevelop_completion.bash
}

kdevelop_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    freedesktop-mime_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

kdevelop_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    freedesktop-mime_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

