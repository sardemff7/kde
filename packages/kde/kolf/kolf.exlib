# Copyright 2013, 2017-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
myexparam kf5_min_ver
myexparam qt_min_ver

exparam -v major_version kf_major_version

require kde-apps kde [ kf_major_version=${major_version} translations=k1i8n ]
require freedesktop-desktop gtk-icon-cache

export_exlib_phases pkg_postinst pkg_postrm

SUMMARY="Miniature golf"
DESCRIPTION="
The game is played from an overhead view, with a short bar representing the golf club. Kolf features
many different types of objects, such water hazards, slopes, sand traps, and black holes (warps),
among others."
HOMEPAGE+=" https://apps.kde.org/${PN}/"

LICENCES="GPL-2 FDL-1.2 LGPL-2"
MYOPTIONS=""

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

# NOTE: Annoyingly upstream bundles an old, modified copy of dev-libs/Box2D
DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:${major_version}[>=${KF5_MIN_VER}]
    build+run:
        kde-frameworks/kcompletion:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kio:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kjobwidgets:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/ktextwidgets:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:${major_version}[>=${KF5_MIN_VER}]
        x11-libs/qtbase:${major_version}[>=${QT_MIN_VER}]
"

if ever at_least 24.04.80 ; then
    CMAKE_SRC_CONFIGURE_PARAMS+=(
        # TODO: Add app-arch/p7zip but the 7zip versions are confusing and
        # Find7Zip module doesn't work with our p7zip yet; falls back to gzip
        -DCMAKE_DISABLE_FIND_PACKAGE_7Zip:BOOL=TRUE
    )
fi

if ever at_least 24.01.75 ; then
    DEPENDENCIES+="
        build+run:
            kde/libkdegames:5[>=24.01.75] [[ note = [ aka 6.0.0 ] ]]
    "
else
    DEPENDENCIES+="
        build+run:
            kde/libkdegames:5[>=21.4.0&<24] [[ note = [ aka 7.3.0 ] ]]
    "
fi

kolf_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

kolf_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

