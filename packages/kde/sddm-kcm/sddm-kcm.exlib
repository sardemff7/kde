# Copyright 2014-2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
myexparam kf5_min_ver
myexparam qt_min_ver

exparam -v major_version kf_major_version

require plasma kde [ kf_major_version=${major_version} translations='ki18n' ]

SUMMARY="KDE config module for SDDM"

LICENCES="GPL-2"
SLOT="4"
MYOPTIONS=""

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build+run:
        kde-frameworks/karchive:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kauth:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcmutils:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kio:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/knewstuff:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kservice:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:${major_version}[>=${KF5_MIN_VER}]
        x11-libs/qtbase:${major_version}[>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:${major_version}[>=${QT_MIN_VER}]
    run:
        kde/kde-cli-tools:4 [[ note = [ kcmshell5 ] ]]
    recommendation:
        sys-auth/polkit-kde-agent[>=5.1.95] [[ description = [ Needed for saving settings requiring authentication ] ]]
"

if [[ ${major_version} == 6 ]] ; then
    :
else
    DEPENDENCIES+="
        build+run:
            kde-frameworks/kconfigwidgets:${major_version}[>=${KF5_MIN_VER}]
            kde-frameworks/kdeclarative:5[>=${KF5_MIN_VER}]
            kde-frameworks/kpackage:${major_version}[>=${KF5_MIN_VER}]
            kde-frameworks/kxmlgui:${major_version}[>=${KF5_MIN_VER}]
    "
fi

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DWAYLAND_SESSIONS_DIR=/usr/share/wayland-sessions
    -DXSESSIONS_DIR=/usr/share/xsessions
)

