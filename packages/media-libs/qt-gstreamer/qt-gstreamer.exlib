# Copyright 2012 Johannes Nixdorf <mixi@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require cmake

export_exlib_phases src_prepare src_compile src_install

SUMMARY="C++ bindings for GStreamer with a Qt-style API"
HOMEPAGE="http://gstreamer.freedesktop.org/modules/qt-gstreamer.html"

DOWNLOADS="http://gstreamer.freedesktop.org/src/${PN}/${PNV}.tar.xz"

LICENCES="LGPL-2.1"
SLOT="0"
MYOPTIONS="doc"

DEPENDENCIES="
    build:
        virtual/pkg-config
        doc? ( app-doc/doxygen )
    build+run:
        dev-libs/boost[>=1.39]
        dev-libs/glib:2
        media-libs/gstreamer:1.0[>=1.0.0]
        media-plugins/gst-plugins-base:1.0[>=1.0.0]
        sys-devel/bison
        sys-devel/flex
        x11-dri/mesa
        x11-libs/qtbase:5[>=5.0.0]
        x11-libs/qtdeclarative:5[>=5.0.0]
"

# Fail to build with newer Qt versions, I could probably fix them, but the
# project is basically abandoned.
RESTRICT="test"

CMAKE_SRC_CONFIGURE_PARAMS=(
    # x11-libs/qtquick1:5 will be deprecated with 5.6, QtQuick2/QML
    # (x11-libs/qtdeclarative:5) is already supported
    -DCMAKE_DISABLE_FIND_PACKAGE_Qt5Declarative:BOOL=TRUE
    -DQT_VERSION=5
    -DQTGSTREAMER_CODEGEN:BOOL=TRUE
)

CMAKE_SRC_CONFIGURE_TESTS+=( '-DTGSTREAMER_TESTS:BOOL=TRUE -DQTGSTREAMER_TESTS:BOOL=FALSE' )

qt-gstreamer_src_prepare() {
    cmake_src_prepare
    # disable failing tests, https://bugzilla.gnome.org/show_bug.cgi?id=710370
    edo sed -e "/^qgst_test(discoverertest)/d" \
            -e "/^qgst_test(refpointertest)/d" \
            -i "${CMAKE_SOURCE}"/tests/auto/CMakeLists.txt
    edo sed -e "/^add_subdirectory(compilation)/d" \
            -i "${CMAKE_SOURCE}"/tests/CMakeLists.txt
}

qt-gstreamer_src_compile() {
    default
    option doc && emake doc
}

qt-gstreamer_src_install() {
    default
    option doc && dodoc -r doc
}

