# Copyright 2020 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=ksnip pn=kImageAnnotator tag=v${PV} ]
require cmake
require xdummy [ phase=test ]

SUMMARY="Tool for annotating images"

LICENCES="LGPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    ( providers: qt5 qt6 ) [[ number-selected = at-least-one ]]
"

QT_MIN_VER="5.15"

DEPENDENCIES="
    build:
        providers:qt5? (
            x11-libs/qttools:5[>=${QT_MIN_VER}] [[ note = [ Qt5LinguistTools ] ]]
        )
        providers:qt6? (
            x11-libs/qttools:6[>=${QT_MIN_VER}] [[ note = [ Qt6LinguistTools ] ]]
        )
    build+run:
        media-libs/kcolorpicker[>=0.3.0][providers:qt5?][providers:qt6?]
        x11-libs/libX11
        providers:qt5? (
            x11-libs/qtbase:5[>=${QT_MIN_VER}][gui]
            x11-libs/qtsvg:5[>=${QT_MIN_VER}]
        )
        providers:qt6? (
            x11-libs/qtbase:6[>=${QT_MIN_VER}]
            x11-libs/qtsvg:6[>=${QT_MIN_VER}]
        )
    test:
        dev-cpp/gtest

"

DEFAULT_SRC_PREPARE_PATCHES+=(
    "${FILES}"/0001-Remove-usage-of-QTEST_ADD_GPU_BLACKLIST_SUPPORT_DEFS.patch
    "${FILES}"/0001-Replace-QLatin1Literal-with-QStringLiteral.patch
)

_kimageannotator_conf() {
    local cmake_params=(
        -DBUILD_EXAMPLE:BOOL=FALSE
        -DBUILD_SHARED_LIBS:BOOL=TRUE
        -DKIMAGEANNOTATOR_LANG_INSTALL_DIR:PATH=/usr/share/kImageAnnotator/translations
        $(expecting_tests -DBUILD_TESTS:BOOL=TRUE -DBUILD_TESTS:BOOL=FALSE)
        "$@"
    )

    ecmake "${cmake_params[@]}"
}

src_prepare() {
    cmake_src_prepare

    # TODO: Upstream, fix install path for translation
    edo sed \
        -e 's:${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_DATAROOTDIR}:${CMAKE_INSTALL_DATAROOTDIR}:g' \
        -i CMakeLists.txt
}

src_configure() {
    if option providers:qt5 ; then
        edo mkdir "${WORKBASE}"/qt5-build
        edo pushd "${WORKBASE}"/qt5-build
        _kimageannotator_conf -DBUILD_WITH_QT6:BOOL=FALSE
        edo popd
    fi

    if option providers:qt6 ; then
        edo mkdir "${WORKBASE}"/qt6-build
        edo pushd "${WORKBASE}"/qt6-build
        _kimageannotator_conf -DBUILD_WITH_QT6:BOOL=TRUE
        edo popd
    fi
}

src_compile() {
    if option providers:qt5 ; then
        edo pushd "${WORKBASE}"/qt5-build
        ecmake_build
        edo popd
    fi

    if option providers:qt6 ; then
        edo pushd "${WORKBASE}"/qt6-build
        ecmake_build
        edo popd
    fi
}

src_test() {
    xdummy_start

    if option providers:qt5 ; then
        edo pushd "${WORKBASE}"/qt5-build/tests
        ectest
        edo popd
    fi

    if option providers:qt6 ; then
        edo pushd "${WORKBASE}"/qt6-build/tests
        ectest
        edo popd
    fi

    xdummy_stop
}

src_install() {
    if option providers:qt5 ; then
        edo pushd "${WORKBASE}"/qt5-build
        ecmake_install
        edo popd
    fi

    if option providers:qt6 ; then
        edo pushd "${WORKBASE}"/qt6-build
        ecmake_install
        edo popd
    fi

    edo pushd "${CMAKE_SOURCE}"
    emagicdocs
    edo popd
}

