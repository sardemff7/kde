# Copyright 2011 Sterling X. Winter <replica@exherbo.org>
# Copyright 2016-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
myexparam kf5_min_ver
myexparam qt_min_ver

exparam -v major_version kf_major_version

require kde.org [ subdir="${PN}/${PV}/src" ]
require kde [ kf_major_version=${major_version} translations='ki18n' ]

export_exlib_phases src_test

SUMMARY="Powerful batch renamer for KDE"
DESCRIPTION="
KRename is a powerful batch renamer for KDE. It allows you to easily rename hundreds or even more
files in one go. The filenames can be created using parts of the original filename, by numbering
the files, or from file metadata like creation date or EXIF information of an image.
"
HOMEPAGE="https://userbase.kde.org/KRename"

LICENCES="BSD-3 [[ note = [ cmake scripts ] ]] GPL-2"
SLOT="0"
MYOPTIONS="
    pdf [[ description = [ Build plugin to use information from PDFs via libpodofo to rename a file ] ]]
    taglib     [[ description = [ Support for extracting metadata from music files ] ]]
"

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build:
        sys-devel/gettext
        virtual/pkg-config
    build+run:
        graphics/exiv2:=[>=0.27]
        kde-frameworks/kcompletion:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kio:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kitemviews:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kjobwidgets:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kservice:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:${major_version}[>=${KF5_MIN_VER}]
        media-libs/freetype:2
        x11-libs/qtbase:${major_version}[>=${QT_MIN_VER}]
        pdf? ( app-text/podofo:= )
        taglib? ( media-libs/taglib:=[>=1.5] )
"

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS=(
    'pdf PoDoFo'
    Taglib
)

if ever at_least 5.0.80 ; then
    DEPENDENCIES+="
        build+run:
            kde-frameworks/karchive:${major_version}
            x11-libs/qt5compat:6[>=${QT_MIN_VER}]
            x11-libs/qtdeclarative:${major_version}[>=${QT_MIN_VER}]
    "
else
    DEPENDENCIES+="
        build+run:
            kde-frameworks/kjs:5
    "
fi

krename_src_test() {
    export QT_QPA_PLATFORM=vnc:port=0

    cmake_src_test
}

