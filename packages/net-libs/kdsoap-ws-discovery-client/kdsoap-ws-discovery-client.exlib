# Copyright 2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

if ever at_least 0.4.0 ; then
    require kde.org [ subdir=${PN} ] cmake
else
    require github [ user=caspermeijn ] cmake

    # git archive --format=tar --prefix=${PN}-${PV}_preyyyymmdd/ HEAD | xz > ${PN}-${PV}_preyyyymmdd.tar.xz
    DOWNLOADS="http://dev.exherbo.org/distfiles/${PN}/${PNV}.tar.xz"
fi

export_exlib_phases src_prepare

SUMMARY="A WS-Discovery client library based on the KDSoap library"

LICENCES="CC0 GPL-3"
SLOT="0"
MYOPTIONS="doc"

DEPENDENCIES="
    build:
        kde-frameworks/extra-cmake-modules[>=5.54.0]
        doc? (
            app-doc/doxygen
        )
    build+run:
"

if ever at_least 0.4.0 ; then
    DEPENDENCIES+="
        build:
            doc? (
                x11-libs/qttools:6
            )
        build+run:
            net-libs/kdsoap[>=2.0.0][providers:qt6]
            x11-libs/qtbase:6[>=5.9.0]
    "

    CMAKE_SRC_CONFIGURE_PARAMS+=(
        -DBUILD_WITH_QT6:BOOL=TRUE
    )
else
    DEPENDENCIES+="
        build:
            doc? (
                x11-libs/qttools:5
            )
        build+run:
            net-libs/kdsoap[>=1.9.0][providers:qt5(+)]
            x11-libs/qtbase:5[>=5.9.0]
    "
fi

kdsoap-ws-discovery-client_src_prepare() {
    cmake_src_prepare

    edo sed \
        -e "/DESTINATION /s|KDSoapWSDiscoveryClient|${PNVR}|" \
        -i docs/CMakeLists.txt
}

CMAKE_SRC_CONFIGURE_PARAMS+=( -DKDE_INSTALL_DATAROOTDIR:PATH=/usr/share )
CMAKE_SRC_CONFIGURE_OPTION_BUILDS+=( 'doc QCH' )
CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=( 'doc Doxygen' )
CMAKE_SRC_CONFIGURE_TESTS+=( '-DBUILD_TESTING:BOOL=TRUE -DBUILD_TESTING:BOOL=FALSE' )

