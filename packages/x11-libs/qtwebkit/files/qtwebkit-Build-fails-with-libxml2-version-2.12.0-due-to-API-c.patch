Upstream: no, original is archived
Source: Borrowed from https://github.com/movableink/webkit/commit/df49bfc4c93001970c9b9266903ee7e8804fb576

From f2541faf05c9efd1acd7844a5d656061837e65bd Mon Sep 17 00:00:00 2001
From: Adrian Perez de Castro <aperez@igalia.com>
Date: Mon, 20 Nov 2023 07:42:30 -0800
Subject: [PATCH] Build fails with libxml2 version 2.12.0 due to API change
 https://bugs.webkit.org/show_bug.cgi?id=265128

Reviewed by Philippe Normand.

Starting with libxml2 2.12.0, the API has changed the const-ness of the
xmlError pointers, which results in a build error due to a mismatched
type in the parsing error callback. This papers over the difference by
using preprocessor conditionals.

* Source/WebCore/xml/XSLTProcessor.h: Use const when building against
  libxml2 2.12.0 or newer.
* Source/WebCore/xml/XSLTProcessorLibxslt.cpp:
(WebCore::XSLTProcessor::parseErrorFunc): Ditto.

Canonical link: https://commits.webkit.org/270977@main
---
 Source/WebCore/xml/XSLTProcessor.h          | 4 ++++
 Source/WebCore/xml/XSLTProcessorLibxslt.cpp | 4 ++++
 2 files changed, 8 insertions(+)

diff --git a/Source/WebCore/xml/XSLTProcessor.h b/Source/WebCore/xml/XSLTProcessor.h
index cffd1c45d558..88094c0256b7 100644
--- a/Source/WebCore/xml/XSLTProcessor.h
+++ b/Source/WebCore/xml/XSLTProcessor.h
@@ -64,7 +64,11 @@ public:
 
     void reset();
 
+#if LIBXML_VERSION >= 21200
+    static void parseErrorFunc(void* userData, const xmlError*);
+#else
     static void parseErrorFunc(void* userData, xmlError*);
+#endif
     static void genericErrorFunc(void* userData, const char* msg, ...);
     
     // Only for libXSLT callbacks
diff --git a/Source/WebCore/xml/XSLTProcessorLibxslt.cpp b/Source/WebCore/xml/XSLTProcessorLibxslt.cpp
index 8587b006cdd7..bd54afaf6cfa 100644
--- a/Source/WebCore/xml/XSLTProcessorLibxslt.cpp
+++ b/Source/WebCore/xml/XSLTProcessorLibxslt.cpp
@@ -78,7 +78,11 @@ void XSLTProcessor::genericErrorFunc(void*, const char*, ...)
     // It would be nice to do something with this error message.
 }
 
+#if LIBXML_VERSION >= 21200
+void XSLTProcessor::parseErrorFunc(void* userData, const xmlError* error)
+#else
 void XSLTProcessor::parseErrorFunc(void* userData, xmlError* error)
+#endif
 {
     PageConsoleClient* console = static_cast<PageConsoleClient*>(userData);
     if (!console)
-- 
2.45.0.rc1

