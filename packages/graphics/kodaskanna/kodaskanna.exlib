# Copyright 2022 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde.org [ unstable=true subdir=${PN}/${PV}/src ] kde
require freedesktop-desktop gtk-icon-cache

export_exlib_phases pkg_postinst pkg_postrm

SUMMARY="A multi-format 1D/2D code scanner"
DESCRIPTION="
Kodaskanna is a utility for reading data from 1D/2D codes (e.g. QR codes or
bar codes) and making the data available for further processing."

LICENCES="
    BSD-3  [[ note = [ cmake scripts ] ]]
    CC-0   [[ note = [ desktop files, appstream metadata, etc ] ]]
    || ( LGPL-2.1 LGPL-3 )
    LGPL-3 [[ note = [ icons ] ]]
"
SLOT="0"
MYOPTIONS=""

QT_MIN_VER="5.15.0"
KF5_MIN_VER="5.85.0"

DEPENDENCIES="
    build+run:
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/purpose:5[>=${KF5_MIN_VER}]
        media-libs/zxing-cpp:=[>=1.2]
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtmultimedia:5[>=${QT_MIN_VER}]
"

kodaskanna_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

kodaskanna_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}
