# Copyright 2015 Bernd Steinhauser <berniyh@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
myexparam kf5_min_ver
myexparam qt_min_ver

exparam -v major_version kf_major_version

require kde-apps kde [ kf_major_version=${major_version} translations='ki18n' ]
require xdummy [ phase=test ] test-dbus-daemon

export_exlib_phases src_test

SUMMARY="Akonadi Calendar Integration"

LICENCES="LGPL-2.1"
SLOT="5"
MYOPTIONS="
    doc [[ description = [ Build API docs in Qt's help format (.qch) ] ]]
"

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build:
        doc? (
            app-doc/doxygen
            x11-libs/qttools:${major_version}   [[ note = qhelpgenerator ]]
        )
    build+run:
        kde-frameworks/kcontacts:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcalendarcore:${major_version}[>=5.63.0]
        kde-frameworks/kcodecs:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kio:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kjobwidgets:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:${major_version}[>=${KF5_MIN_VER}]
        x11-libs/qtbase:${major_version}[>=${QT_MIN_VER}]
        !kde/kalendar[<22.03.80] [[
            description = [ Parts moved from kalendar to akonadi-calendar ]
            resolution = uninstall-blocked-after
        ]]
"

if ever at_least 24.02.0 ; then
    DEPENDENCIES+="
        build+run:
            kde/libkleo[>=${PV}]
            kde/messagelib[>=${PV}]
            kde-frameworks/akonadi-contact:5[>=${PV}]
            kde-frameworks/akonadi-mime:5[>=${PV}]
            kde-frameworks/kcalutils:${major_version}[>=${PV}]
            kde-frameworks/kidentitymanagement:${major_version}[>=${PV}]
            kde-frameworks/kmailtransport:5[>=${PV}]
            kde-frameworks/kmime:${major_version}[>=${PV}]
            server-pim/akonadi:5[>=${PV}]
    "
else
    DEPENDENCIES+="
        build+run:
            kde/libkleo[>=${PV}&<24]
            kde/messagelib[>=${PV}&<24]
            kde-frameworks/akonadi-contact:5[>=${PV}&<24]
            kde-frameworks/akonadi-mime:5[>=${PV}&<24]
            kde-frameworks/kcalutils:5[>=${PV}]
            kde-frameworks/kidentitymanagement:5[>=${PV}]
            kde-frameworks/kmailtransport:5[>=${PV}&<24]
            kde-frameworks/kmime:5[>=${PV}]
            server-pim/akonadi:5[>=${PV}&<24]
    "
fi

CMAKE_SRC_CONFIGURE_OPTION_BUILDS+=( 'doc QCH' )

# Wants to start an akonadi and possibly a MySQL server
RESTRICT="test"

akonadi-calendar_src_test() {
    xdummy_start

    test-dbus-daemon_run-tests cmake_src_test

    xdummy_stop
}

