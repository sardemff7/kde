# Copyright 2014-2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
exparam -v major_version kf_major_version

require kde-frameworks kde [ kf_major_version=${major_version} translations='qt' ]
require test-dbus-daemon xdummy [ phase=test ]

export_exlib_phases src_test

SUMMARY="Hardware integration and detection"
DESCRIPTION="Solid provides the following features for application developers:
- Hardware Discovery
- Power Management
- Network Management"

LICENCES="BSD-3 [[ note = [ cmake scripts ] ]] LGPL-2.1"
MYOPTIONS="
    ios [[ description = [ iOS device support backend ] ]]

    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        sys-devel/bison[>=3.0]
        sys-devel/flex
        virtual/pkg-config
    build+run:
        sys-apps/util-linux
        ios? (
            app-pda/libimobiledevice:1.0
            dev-libs/libplist:2.0
        )
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd[>=167] )
    recommendation:
        sys-apps/udisks:2 [[ description = [ Enable auto-mounting support ] ]]
        sys-apps/upower [[ description = [ Battery info and suspend menus ] ]]
"

# TODO: solid has an optional runtime dependency upon media-player-info:
# MediaPlayerInfo , Enables identification and querying of portable media players,
# <http://www.freedesktop.org/wiki/Software/media-player-info>
# Runtime-only dependency of the udev solid backend. Support for m-p-i is included even if not found during build

if ever at_least 5.245.0 ; then
    :
else
    DEPENDENCIES+="
        build+run:
            x11-libs/qtdeclarative:${major_version}[>=${QT_MIN_VER}]
    "
fi

CMAKE_SRC_CONFIGURE_PARAMS+=(
    # We might want to enable those at a later point, but right now they are marked WIP
    -DWITH_NEW_POWER_ASYNC_API:BOOL=FALSE
    -DWITH_NEW_POWER_ASYNC_FREEDESKTOP:BOOL=FALSE
    -DWITH_NEW_SOLID_JOB:BOOL=FALSE
)

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'ios IMobileDevice'
    'ios PList'
)

# Needs to access to system dbus to get devices via udisks
CMAKE_SRC_TEST_PARAMS+=( -E solidmttest )

solid_src_test() {
    xdummy_start

    cmake_src_test

    xdummy_stop
}

