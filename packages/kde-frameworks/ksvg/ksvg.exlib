# Copyright 2023 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-frameworks kde [ kf_major_version=6 translations='ki18n' ]
require xdummy [ phase=test ]

export_exlib_phases src_test

SUMMARY="A library for rendering SVG-based themes with stylesheet recoloring"

LICENCES="
    CC-0  [[ note = [ .gitlab-ci.yml ] ]]
    || ( GPL-2 GPL-3 )
    || ( LGPL-2.0 LGPL-2.1 LGPL-3 )
"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        kde-frameworks/karchive:6[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:6[>=${KF5_MIN_VER}]
        kde-frameworks/kcolorscheme:6[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:6[>=${KF5_MIN_VER}]
        kde-frameworks/kguiaddons:6[>=${KF5_MIN_VER}]
        kde-frameworks/kirigami:6[>=${KF5_MIN_VER}]
        x11-libs/qtbase:6[>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:6[>=${QT_MIN_VER}]
        x11-libs/qtsvg:6[>=${QT_MIN_VER}]
"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DBUILD_COVERAGE:BOOL=FALSE
    # Would build a tool split-plasma-svgs, which isn't used for anything
    # at the moment.
    -DBUILD_TOOLS:BOOL=FALSE
)

ksvg_src_test() {
    xdummy_start

    cmake_src_test

    xdummy_stop
}

