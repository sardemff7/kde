# Copyright 2019-2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
exparam -v major_version kf_major_version

require kde-frameworks [ docs=false ] kde [ kf_major_version=${major_version} ]
require xdummy [ phase=test ] test-dbus-daemon

export_exlib_phases src_test

SUMMARY="A QtQuick plugin providing high-performance charts"
DESCRIPTION="
The Quick Charts module provides a set of charts that can be used from QtQuick
applications. They are intended to be used for both simple display of data as
well as continuous display of high-volume data (often referred to as plotters).
The charts use a system called distance fields for their accelerated rendering,
which provides ways of using the GPU for rendering 2D shapes without loss of
quality."

LICENCES="LGPL-2.1"
MYOPTIONS="examples"

DEPENDENCIES="
    build+run:
        x11-libs/qtdeclarative:${major_version}[>=${QT_MIN_VER}]
        examples? (
            kde-frameworks/kdeclarative:${major_version}
        )
"

if ever at_least 5.240.0 ; then
    DEPENDENCIES+="
        build+run:
            x11-libs/qtshadertools:6[>=${QT_MIN_VER}]
            examples? ( kde-frameworks/kirigami:6 )
    "
else
    DEPENDENCIES+="
        build+run:
            x11-libs/qtquickcontrols2:5[>=${QT_MIN_VER}]
            examples? ( kde-frameworks/kirigami:2 )
        test:
            dev-lang/glslang
    "
fi

CMAKE_SRC_CONFIGURE_OPTION_BUILDS+=( EXAMPLES )

kquickcharts_src_test() {
    xdummy_start

    test-dbus-daemon_run-tests cmake_src_test

    xdummy_stop
}

