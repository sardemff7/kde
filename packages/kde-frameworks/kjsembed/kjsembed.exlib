# Copyright 2014-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-frameworks [ docs=false ] kde [ translations='ki18n' ]

export_exlib_phases pkg_preinst

SUMMARY="Allows binding a Javascript object to QObject"
DESCRIPTION="
KSJEmbed provides a method of binding JavaScript objects to QObjects,
so you can script your applications."

LICENCES="LGPL-2.1"
MYOPTIONS="doc"

DEPENDENCIES="
    build:
        doc? ( kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}] )
    build+run:
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kjs:5[>=${KF5_MIN_VER}]
        x11-libs/qtsvg:5[>=${QT_MIN_VER}]
        x11-libs/qttools:5[>=${QT_MIN_VER}]
"

kjsembed_pkg_preinst() {
    # Versions < 4.99.0 installed a file whereas later versions install a directory.
    if [[ -f ${ROOT}/usr/include/KF5/KJsEmbed/KJsEmbed ]] ; then
        nonfatal edo rm "${ROOT}"/usr/include/KF5/KJsEmbed/KJsEmbed
    fi
}

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=( 'doc KF5DocTools' )

