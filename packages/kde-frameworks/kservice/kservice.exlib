# Copyright 2014 Bernd Steinhauser <berniyh@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
exparam -v major_version kf_major_version

require kde-frameworks kde [ kf_major_version=${major_version} translations='ki18n' ]
require xdummy [ phase=test ] test-dbus-daemon

export_exlib_phases src_test

SUMMARY="Plugin framework for desktop services"
DESCRIPTION="
KService provides a plugin framework for handling desktop services. Services can
be applications or libraries. They can be bound to MIME types or handled by
application specific code.
"

LICENCES="GPL-2 GPL-3 LGPL-2.1"
MYOPTIONS=""

DEPENDENCIES="
    build:
        doc? ( kde-frameworks/kdoctools:${major_version}[>=${KF5_MIN_VER}] )
    build+run:
        kde-frameworks/kconfig:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:${major_version}[>=${KF5_MIN_VER}]
"

if ever at_least 5.240.0 ; then
    :
else
    DEPENDENCIES+="
        build:
            sys-devel/bison[>=3.0]
            sys-devel/flex
        build+run:
            kde-frameworks/kdbusaddons:${major_version}[>=${KF5_MIN_VER}]
    "
fi

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=( "doc KF${major_version}DocTools" )

CMAKE_SRC_TEST_PARAMS+=(
    # Somehow two tests started to fail when ran in parallel
    -j1
    # Fail if konsole is installed
    -E ktoolinvocation_x11test
)

kservice_src_test() {
    xdummy_start
    test-dbus-daemon_run-tests cmake_src_test
    xdummy_stop
}

