# Copyright 2014-2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
exparam -v major_version kf_major_version

require kde-frameworks kde [ kf_major_version=${major_version} ]
require xdummy [ phase=test ]
require freedesktop-desktop
require alternatives

export_exlib_phases src_test src_install

SUMMARY="Utilities for graphical user interfaces"
DESCRIPTION="
The KDE GUI addons provide utilities for graphical user interfaces in the areas
of colors, fonts, text, images, keyboard input."

LICENCES="GPL-2 LGPL-2.1"
MYOPTIONS="
    wayland
    X [[ presumed = true ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        wayland? (
            sys-libs/wayland[>=1.9]
            x11-libs/qtwayland:${major_version}[>=${QT_MIN_VER}]
        )
        X? (
            x11-libs/libX11
            x11-libs/libxcb
        )
"

if ever at_least 5.240.0 ; then
    DEPENDENCIES+="
        build:
            wayland? ( kde/plasma-wayland-protocols[>=1.10.0] )
    "
else
    DEPENDENCIES+="
        build:
            wayland? ( kde/plasma-wayland-protocols[>=1.7.0] )
        build+run:
            X? ( x11-libs/qtx11extras:5[>=${QT_MIN_VER}] )
    "
fi

CMAKE_SRC_CONFIGURE_PARAMS+=(
    # It could be used to avoid collisions with its :5 counterpart, but we
    # just create light alternatives for the handlers.
    -DBUILD_GEO_SCHEME_HANDLER:BOOL=TRUE
    -WITH_DBUS:BOOL=TRUE
)
CMAKE_SRC_CONFIGURE_OPTION_WITHS+=(
    WAYLAND
    'X X11'
)

kguiaddons_src_test() {
    xdummy_start

    cmake_src_test

    xdummy_stop
}

kguiaddons_src_install() {
    kde_src_install

    local host=$(exhost --target)

    alternatives_for _${host}_${PN} ${SLOT} ${SLOT} \
        /usr/$(exhost --target)/bin/kde-geo-uri-handler kde-geo-uri-handler-${SLOT}

    alternatives_for _${PN} ${SLOT} ${SLOT} \
        /usr/share/applications/google-maps-geo-handler.desktop google-maps-geo-handler.desktop-${SLOT} \
        /usr/share/applications/openstreetmap-geo-handler.desktop openstreetmap-geo-handler.desktop-${SLOT} \
        /usr/share/applications/qwant-maps-geo-handler.desktop qwant-maps-geo-handler.desktop-${SLOT} \
        /usr/share/applications/wheelmap-geo-handler.desktop wheelmap-geo-handler.desktop-${SLOT}
}

