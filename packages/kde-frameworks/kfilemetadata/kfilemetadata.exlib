# Copyright 2014-2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
exparam -v major_version kf_major_version

require kde-frameworks kde [ kf_major_version=${major_version} translations='ki18n' ]
require ffmpeg [ with_opt=true ]

export_exlib_phases src_test

SUMMARY="A library for extracting file metadata"

LICENCES="BSD-3 [[ note = [ cmake scripts ] ]] LGPL-2 LGPL-2.1 LGPL-3"

MYOPTIONS="
    appimage [[ description = [ Support for extracting metadata from AppImage files ] ]]
    epub     [[ description = [ Support for extracting metadata from .epub ebook files ] ]]
    exif     [[ description = [ Support for extracting metadata from EXIF headers ] ]]
    ffmpeg   [[ description = [ Support for extracting audio and video metadata ] ]]
    mobipocket [[ description = [ Support for extracting metadata from Mobipocket e-books ] ]]
    pdf      [[ description = [ Support for extracting metadata from pdf files ] ]]
    taglib   [[ description = [ Support for extracting metadata from music files ] ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        kde-frameworks/karchive:${major_version}[>=${KF5_MIN_VER}] [[ note = [ could be optional ] ]]
        kde-frameworks/kcoreaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:${major_version}[>=${KF5_MIN_VER}]
        sys-apps/attr
        appimage? (
            dev-libs/libappimage[>=0.1.10]
            kde-frameworks/kconfig:${major_version}[>=${KF5_MIN_VER}] [[ note = [ could be optional ] ]]
        )
        epub? ( app-text/ebook-tools )
        exif? ( graphics/exiv2:=[>=0.26] )
    test:
        dev-lang/python:*[>=3]
    suggestion:
        app-doc/catdoc [[ description = [ Extract text from office 98 files ] ]]
"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DCMAKE_FIND_ROOT_PATH:PATH="$(ffmpeg_alternatives_prefix);/usr/$(exhost --target)"
)

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'appimage libappimage'
    EPub
    'exif LibExiv2'
    FFmpeg
    'pdf Poppler'
    Taglib
)

if ever at_least 6.1.0 ; then
    DEPENDENCIES+="
        build+run:
            kde-frameworks/kcodecs:${major_version}[>=${KF5_MIN_VER}]
            mobipocket? ( kde/mobipocket:6[>=24] )
            pdf? ( app-text/poppler[>=0.12.1][qt6] )
            taglib? ( media-libs/taglib:=[>=1.12] )
    "

    CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
        'appimage KF6Config'
        'mobipocket QMobipocket6'
    )

    CMAKE_SRC_TEST_PARAMS+=(
        # TODO: usermetadatawritertest doesn't seem to work in paludis' env
        -E '(exiv2extractortest|usermetadatawritertest)'
    )
else
    DEPENDENCIES+="
        build+run:
            mobipocket? ( kde/mobipocket:4[>=2.0] )
            pdf? ( app-text/poppler[>=0.12.1][qt5] )
            taglib? ( media-libs/taglib:=[>=1.11.1] )
    "

    CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
        'appimage KF5Config'
        'mobipocket QMobipocket'
    )
fi

kfilemetadata_src_test() {
    # propertyinfotest hard codes this
    LC_ALL=en_US.UTF-8 cmake_src_test
}

