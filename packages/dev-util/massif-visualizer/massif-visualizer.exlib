# Copyright 2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
myexparam kf5_min_ver
myexparam qt_min_ver

exparam -v major_version kf_major_version

require kde.org [ subdir="${PN}/${PV}/src" ]
require kde [ kf_major_version=${major_version} translations="ki18n" ]
require freedesktop-desktop freedesktop-mime gtk-icon-cache

export_exlib_phases pkg_postinst pkg_postrm

SUMMARY="A visualizer for Valgrind Massif data files"
DESCRIPTION="
Massif Visualizer is a tool that - who'd guess that - visualizes massif data.
You run your application in Valgrind with --tool=massif and then open the
generated massif.out.%pid in the visualizer. Gzip or Bzip2 compressed massif
files can also be opened transparently."

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="
    callgraph [[
        description = [ View callgraph dot file in the KGraphViewer part ]
    ]]
"

QT_MIN_VER="5.2.0"
exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build+run:
        kde-frameworks/karchive:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kio:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kparts:${major_version}[>=${KF5_MIN_VER}]
        x11-libs/qtbase:${major_version}[>=${QT_MIN_VER}]
        x11-libs/qtsvg:${major_version}[>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:${major_version}[>=${QT_MIN_VER}]
    run:
        dev-util/valgrind
"

if ever at_least 0.7.80 ; then
    DEPENDENCIES+="
        build+run:
            kde/kdiagram:6[>=3.0.0]
            kde-frameworks/kcompletion:6[>=${KF5_MIN_VER}]
            x11-libs/qt5compat:6[>=${QT_MIN_VER}]
            callgraph? ( media-gfx/kgraphviewer[>=2.5.0] )
    "
else
    DEPENDENCIES+="
        build+run:
            kde/kdiagram:0[>=2.6.0]
            callgraph? ( media-gfx/kgraphviewer[>=2.3.90] )
    "
fi

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS=(
    'callgraph KGraphViewerPart'
)

massif-visualizer_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    freedesktop-mime_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

massif-visualizer_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    freedesktop-mime_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

