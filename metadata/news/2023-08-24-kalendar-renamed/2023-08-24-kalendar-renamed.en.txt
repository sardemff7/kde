Title: kde/kalendar has been renamed to kde/merkuro
Author: Heiko Becker <heirecka@exherbo.org>
Content-Type: text/plain
Posted: 2022-08-24
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: kde/kalendar

Please install kde/merkuro and *afterwards* uninstall kde/kalendar.

1. Take note of any packages depending on kde/kalendar
cave resolve \!kde/kalendar

2. Install kde/merkuro:
cave resolve kde/merkuro -x

3. Re-install the packages from step 1.

4. Uninstall kde/kalendar
cave resolve \!kde/kalendar -x

Do it in *this* order or you'll potentially *break* your system.
